classdef BoreholeFunction < Function
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        fun
        grad
    end
    
    methods
        function u = BoreholeFunction()
            
            u.evaluationAtMultiplePoints = true;
            u.dim = 8;
            u.outputSize = [1,1];
            
            %%%%%%%%%%%%%%%%%%%%%%%%
            
            X = cell(8,1);
            X{1}=NormalRandomVariable(0.1,0.0161812);
            X{2}=NormalRandomVariable(0,1);
            X{3}=UniformRandomVariable(63070,115600);
            X{4}=UniformRandomVariable(990,1110);
            X{5}=UniformRandomVariable(63.1,116);
            X{6}=UniformRandomVariable(700,820);
            X{7}=UniformRandomVariable(1120,1680);
            X{8}=UniformRandomVariable(9855,12045);
            
            X = RandomVector(X);
            RV = getStandardRandomVector(X);
            u.measure = ProductMeasure(RV);
            
            
            syms x1 x2 x3 x4 x5 x6 x7 x8
            syms y1 y2 y3 y4 y5 y6 y7 y8
            
            % X{1}=NormalRandomVariable(0.1,0.0161812);
            y1 = 0.1 + x1*0.0161812;
            % X{2}=NormalRandomVariable(0,1);
            y2 = x2;
            % X{3}=UniformRandomVariable(63070,115600);
            a = 63070;
            b = 115600;
            y3 = a + (x3+1)*(b-a)/2;
            % X{4}=UniformRandomVariable(990,1110);
            a = 990;
            b = 1110;
            y4 = a + (x4+1)*(b-a)/2;
            % X{5}=UniformRandomVariable(63.1,116);
            a = 63.1;
            b = 116;
            y5 = a + (x5+1)*(b-a)/2;
            % X{6}=UniformRandomVariable(700,820);
            a = 700;
            b = 820;
            y6 = a + (x6+1)*(b-a)/2;
            % X{7}=UniformRandomVariable(1120,1680);
            a = 1120;
            b = 1680;
            y7 = a + (x7+1)*(b-a)/2;
            % X{8}=UniformRandomVariable(9855,12045);
            a = 9855;
            b = 12045;
            y8 = a + (x8+1)*(b-a)/2;
            
            
            g = 2*pi*y3.*(y4-y6)./(...
                (7.71+1.0056*y2 - log(y1) ).*...
                (1+...
                2*y7.*y3./(y1.^2)./y8./(7.71+1.0056*y2 - log(y1) ) + ...
                y3./y5)...
                );
            
            
            g = matlabFunction(g);
            g1 = matlabFunction(diff(g,x1));
            g2 = matlabFunction(diff(g,x2));
            g3 = matlabFunction(diff(g,x3));
            g4 = matlabFunction(diff(g,x4));
            g5 = matlabFunction(diff(g,x5));
            g6 = matlabFunction(diff(g,x6));
            g7 = matlabFunction(diff(g,x7));
            g8 = matlabFunction(diff(g,x8));
            
            clear x1 x2 x3 x4 x5 x6 x7 x8
            clear y1 y2 y3 y4 y5 y6 y7 y8
            
            u.fun = @(x) g(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g1 = @(x) g1(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g2 = @(x) g2(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g3 = @(x) g3(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g4 = @(x) g4(x(:,1),x(:,2),x(:,3),x(:,5),x(:,7),x(:,8));
            g5 = @(x) g5(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g6 = @(x) g6(x(:,1),x(:,2),x(:,3),x(:,5),x(:,7),x(:,8));
            g7 = @(x) g7(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            g8 = @(x) g8(x(:,1),x(:,2),x(:,3),x(:,4),x(:,5),x(:,6),x(:,7),x(:,8));
            
            u.grad = @(x) [g1(x),g2(x),g3(x),g4(x),g5(x),g6(x),g7(x),g8(x)];
            
        end
        
        function y = eval(u,x)
            y = u.fun(x);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function g = evalGradient(u,x)
            
            g = u.grad(x);
            
            
        end
        
    end
    
end

