classdef cosOfNorm < Function
    %
    
    
    properties
        fun
        grad
    end
    
    methods
        function u = cosOfNorm(d, type)
            
            if nargin<2
                type = 1;
            end
            u.evaluationAtMultiplePoints = true;
            u.dim = d;
            u.outputSize = [1,1];
            
            % The random variable
            X = cell(d,1);
            for i=1:d
                X{i}=NormalRandomVariable(0,1);
            end
            u.measure = ProductMeasure(X);
            
            omega = 1;
            % Define the fun
            if type==1
                u.fun = @(x) cos( omega*sqrt(sum(x.^2,2)) );
                u.grad = @(x) -omega*x.*( sin( omega*sqrt(sum(x.^2,2)) )./sqrt(sum(x.^2,2)) );
            elseif type==2
                u.fun = @(x) cos( sqrt(sum(x.^2,2)) ).*exp(x(:,d));
                u.grad = @(x) -x.*( sin( sqrt(sum(x.^2,2)) )./sqrt(sum(x.^2,2)).*exp(x(:,d)) ) ...
                              + [zeros( size(x,1), d-1 ) , cos( sqrt(sum(x.^2,2)) ).*exp(x(:,d))];
            end
        end
        
        function y = eval(u,x)
            y = u.fun(x);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function g = evalGradient(u,x)
            
            g = u.grad(x);
            
            
        end
        
    end
    
end

