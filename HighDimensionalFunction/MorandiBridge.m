classdef MorandiBridge < Function
    % Smallest resonant frequency of a Morandi bridge
    
    properties
        
        MeshSize  % diameter of the largest element
        nbElements % parametric dimension (= number of elements)
        indEigs 
        
        Sigma12 % Square root Sigma such that Sigma = Sigma12 * Sigma12'
        
    end
    
    properties (Access = private)
        % Those properties are meant to speedup the code (precomputations)
        Geometry  % geometry of the bridge
        PDEmodel  % model for the PDE to solve
        CenterOfElements % List containing the center of the elements
        deltaA  % non-assembled stiffness matrix: nbElem x 1 cells containing matrices you have to sum up to get A
        FEM
        TR
    end
    
    methods
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = MorandiBridge(d,ind,hmax)
            
            % Definition of the model
            if nargin<1
                d = []; % Expansion in the Kahrunen Loeve expansion (= dimension of the problem)
            end
            if nargin<2
                ind = 1; % Index of the eigenvalue (1 = smallest, [1:5] = the 5 smallest)
            end
            if nargin<3
                hmax = 3; % Mesh size (=3 by default)
            end
            
            % Properties
            self.evaluationAtMultiplePoints = false;
            self.outputSize = [1,length(ind)];
            self.indEigs = ind;
            
            % Initialization
            self = initGeo(self);
            self = initPDEmodel(self);
            self = initMesh(self,hmax);
            self = initFEM(self);
            self = self.initDeltaA(); % this takes some times... 
            
            % Parameter of the covariance function
            %   (x,y) ->  sigma^2 * exp[ -( ||x-y||_2/l0 )^alpha ]
            %
            % A good choice: alpha = 2; sigma = sqrt(5); l0 = 20; dim = 50;
            
            alpha = 2;
            sigma = sqrt(5);
            l0 = 20;
            
            self = self.initYoungsModulusField(alpha,sigma,l0,d);
            
        end %endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function [lambda,x,U] = eval(self,x)
            %  function [lambda,x,U] = eval(self,x)
            % Computes the eigenvalue(s) lambda associated with the 
            % parameter x. Returns also the corresponding eigenvector U
            % x: N-by-dim double
            % lambdra: N-by-NbEigenValues double
            % U: N-by-1 cell of dof-by-NbEigenValues double
            %  or, if N=1,
            % U: dof-by-NbEigenValues double
            %
            % Input arguments
            if nargin<2
                x = self.measure.random();
            end
            if size(x,1)>1
                lambda = zeros(size(x,1),self.outputSize(2));
                U = cell(size(x,1),self.outputSize(2));
                for i=1:size(x,1)
                    [lambda(i),~,U{i}] = self.eval(x(i,:));
                end
                return
            end
            % Assemble the stiffness matrix K
            logE = self.Sigma12*x';
            E = exp(logE);
            if isempty(self.deltaA)
                % Hooke tensor for plain-stress problems, see https://www.mathworks.com/help/pde/ug/3-d-linear-elasticity-equations-in-toolbox-form.html?searchHighlight=linear%2520elasticity&s_tid=doc_srchtitle#bumr56f-1
                eta = 0.3; % Poissons ratio
                hookeEta = [ 1/(1-eta^2) 0 0 eta/(1-eta^2) ;...
                    0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                    0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                    eta/(1-eta^2) 0 0 1/(1-eta^2)] ;
                hookeEta = reshape(hookeEta,[2,2,2,2]);
                hookeEta = permute(hookeEta,[1 3 2 4]); % I know... it is crazy... but that works!
                hookeEta = reshape(hookeEta,[4,4]);
                hookeTensor = @(Ex) repmat(hookeEta(:),1,length(Ex)) * spdiags(Ex(:),0,length(Ex),length(Ex)) ;
                c = @(region,state) hookeTensor( E(self.TR.pointLocation(region.x(:),region.y(:))) );
                specifyCoefficients(self.PDEmodel,'m',0,'d',0,'c',c,'a',0,'f',[0;0]);
                
                K = assembleFEMatrices(self.PDEmodel , 'nullspace');
                K = K.Kc;
            else
                K = cellfun(@(x)x(:),self.deltaA,'UniformOutput',0);
                K = [K{:}];
                K = reshape( K*sparse(E) , size(self.FEM.M));
            end
            
            
            % Find the eigenvalues of (K,M)
            [U,lambda] = eigs(K, self.FEM.M ,max(self.indEigs),'smallestabs');
            U = U(:,self.indEigs);
            lambda = diag(lambda);
            lambda = lambda(self.indEigs);
            
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function [grad,x,U] = evalGradient(self,x,U)
            
            % Input arguments
            if nargin<2
                x = self.measure.random();
            end
            if size(x,1)>1
                grad = zeros(size(x,1),self.dim);
                U = cell(size(x,1),self.outputSize(2));
                for i=1:size(x,1)
                    [g,~,U{i}] = self.evalGradient(x(i,:));
                    grad(i,:) = g;
                end
                return
            end
            
            if nargin<3
                [~,~,U] = self.eval(x);
            end
            
            logE = self.Sigma12*x';
            UMU = diag(U'*self.FEM.M*U);
            grad = cellfun(@(X,logE) diag( U'*(X*exp(logE))*U )./UMU , self.deltaA , num2cell(logE),'UniformOutput',0);
            grad = [grad{:}];
            grad = grad*self.Sigma12;
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function [logE,x] = logE(self,x)
            % Draw the parameter (the log of the Young's Modulus)
            if nargin<2
                x = self.measure.random();
            end
            logE = self.Sigma12*x';
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function plotMode(self,U,logE)
            
            if nargin<2
                [~,x,U] = self.eval();
                logE = self.Sigma12*x';
            end
            
            
            
            factor = 5/max(U(:));
            U = self.FEM.B * U;
            U = reshape(U,length(U)/2,2 )';
            [p,k,t] = meshToPet(self.PDEmodel.Mesh);
            p = p+U*factor;
            
            
            if nargin~=2
                t(4,:)=[];
                x=p(1,:);
                y=p(2,:);
                P=[x(t(:));y(t(:))];
                T=reshape(1:size(P,2),[3 size(P,2)/3]);
                tmp=repmat(logE(:)',3,1);
                
                trisurf(T',P(1,:),P(2,:),tmp(:))
                
                grid off
                view(0,90)
                axis equal
                
                colorbar
                caxis([min(logE) max(logE)])
            else
                pdeplot(p,k,t)
            end
            
            axis([-95 95 -10 95])
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function plot(self,data)
            % Plot a piecewise constance field
            %   - data : element-based field
            
            [p,~,t]= self.PDEmodel.Mesh.meshToPet();
            t(4,:)=[];
            
            x=p(1,:);
            y=p(2,:);
            P=[x(t(:));y(t(:))];
            T=reshape(1:size(P,2),[3 size(P,2)/3]);
            
            tmp=repmat(data(:)',3,1);
            trisurf(T',P(1,:),P(2,:),tmp(:))
            
            grid off
            view(0,90)
            axis equal
            
            colorbar
            caxis([min(data) max(data)])
            axis([-95 95 -10 95])
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function plotGeometry(self)
            
            pdegplot(self.Geometry,'EdgeLabels','off','FaceLabels','off')
            axis equal
            axis([-95 95 -10 95])
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function plotMesh(self)
            
            pdemesh(self.PDEmodel,'ElementLabels','off')
            axis equal
            axis([-95 95 -10 95])
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
        end % endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
    end % endMethods
    methods (Access = private) % Set the initialization functions...
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initGeo(self)
            
            gd = [];
            
            % Socle
            xi = [-12.9;12.9;12.9;-12.9];
            yi = [-5;-5;0;0];
            gd = [gd,[3;4 ; xi ; yi]];
            
            % Pile
            xi = [-12.9;-12.9+4.3;+0.5;-0.5];
            yi = [0;0;90.20;90.20];
            gd = [gd,[3;4 ; xi ; yi]];
            xi = -xi;
            gd = [gd,[3;4 ; xi ; yi]];
            
            % Tablier
            xi = [-75.94;75.94;75.94;-75.94];
            yi = [44.83-4.3;44.83-4.3;44.83;44.83];
            gd = [gd,[3;4 ; xi ; yi]];
            
            xi = [-75.94-11;-75.94;-75.94;-75.94-11];
            yi = [44.83-4.3/2;44.83-4.3;44.83;44.83];
            gd = [gd,[3;4 ; xi ; yi]];
            
            xi = -xi;
            gd = [gd,[3;4 ; xi ; yi]];
            
            % Haubans
            xi = [-74.91;-74.91+3.7;0;-0.5];
            yi = [44.83;44.83;90.20 - 0.5 ; 90.20];
            gd = [gd,[3;4 ; xi ; yi]];
            xi = -xi;
            gd = [gd,[3;4 ; xi ; yi]];
            
            % Renforts
            xi = [-24; -10.5 ;-8.6 ;-20.25];
            yi = [44.83;10.45;10.45+5.5; 44.83];
            gd = [gd,[3;4 ; xi ; yi]];
            xi = -xi;
            gd = [gd,[3;4 ; xi ; yi]];
            xi = [-10.5;10.5;8.6;-8.6];
            yi = [10.45;10.45;10.45+5.5;10.45+5.5];
            gd = [gd,[3;4 ; xi ; yi]];
            
            [geo,bt] = decsg(gd);
            [geo,~] = csgdel(geo,bt);
            self.Geometry = geo;
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initPDEmodel(self)
            
            % Initialize
            self.PDEmodel = createpde(2); % the "2" means we have a set of two equations
            geometryFromEdges(self.PDEmodel,self.Geometry);
            
            % Set the boundary conditions
            applyBoundaryCondition(self.PDEmodel,'dirichlet','Edge',1,'u',[0;0]);
            applyBoundaryCondition(self.PDEmodel,'mixed','Edge',[5,7],'u',0,'EquationIndex',[1]);
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initMesh(self,hmax)
            
            generateMesh(self.PDEmodel,'Hmax',hmax,'GeometricOrder','linear');
            self.nbElements = size(self.PDEmodel.Mesh.Elements,2);
            self.MeshSize = hmax;
            self.TR = triangulation(self.PDEmodel.Mesh.Elements',...
                                    self.PDEmodel.Mesh.Nodes');
            self.CenterOfElements = incenter(self.TR)';
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initYoungsModulusField(self,alpha,sigma,l0,d)
            
            % Distance matrix between the centers of the elements
            DXY = pdist(self.CenterOfElements','euclidean');
            DXY = squareform(DXY);
            
            % Creating the covariance matrix
            Sigma = sigma^2 *exp( -(DXY/l0).^alpha );
            
            % Truncated Kahrunen Loeve decomposition of Sigma (for fast sampling)
            [U,D,~] = svd(Sigma);
            D = diag(D);
            
            % Remove the (almost) zeros eigenmodes
            ind = D<(100*eps*max(D));
            D(ind)=[];
            U(:,ind)=[];
            self.Sigma12 = U*diag(sqrt(D));
            
            % If d is specified, truncate the KL to its first d terms
            if ~(nargin<5 || isempty(d))
                self.Sigma12 = self.Sigma12(:,1:d);
            end
            self.dim = size(self.Sigma12,2);
            
            % Create the germ random variable (std normal in R^d)
            self.measure = ProductMeasure(repmat({NormalRandomVariable(0,1)},1,self.dim));
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initDeltaA(self)
            
            nbElem = self.nbElements;
            selfdeltaA = cell(nbElem,1);
            
            for k=1:nbElem
                E = zeros(nbElem,1);
                E(k) = 1;
                
                eta = 0.3; % Poissons ratio
                hookeEta = [ 1/(1-eta^2) 0 0 eta/(1-eta^2) ;...
                    0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                    0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                    eta/(1-eta^2) 0 0 1/(1-eta^2)] ;
                hookeEta = reshape(hookeEta,[2,2,2,2]);
                hookeEta = permute(hookeEta,[1 3 2 4]); % I know... it is crazy... but that works!
                hookeEta = reshape(hookeEta,[4,4]);
                
                hookeTensor = @(Ex) repmat(hookeEta(:),1,length(Ex)) * spdiags(Ex(:),0,length(Ex),length(Ex)) ;
                c = @(region,state) hookeTensor( E(self.TR.pointLocation(region.x(:),region.y(:))) );
                
                specifyCoefficients(self.PDEmodel,'m',0,'d',0,'c',c,'a',0,'f',[0;0]);
                selfFEM = assembleFEMatrices(self.PDEmodel , 'nullspace');
                selfdeltaA{k} = selfFEM.Kc;
                
            end
            self.deltaA = selfdeltaA;
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function self = initFEM(self)
            
            E = ones(self.nbElements,1);
            eta = 0.3; % Poissons ratio
            hookeEta = [ 1/(1-eta^2) 0 0 eta/(1-eta^2) ;...
                0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                eta/(1-eta^2) 0 0 1/(1-eta^2)] ;
            hookeEta = reshape(hookeEta,[2,2,2,2]);
            hookeEta = permute(hookeEta,[1 3 2 4]); % I know... it is crazy... but that works!
            hookeEta = reshape(hookeEta,[4,4]);
            hookeTensor = @(Ex) repmat(hookeEta(:),1,length(Ex)) * spdiags(Ex(:),0,length(Ex),length(Ex)) ;
            c = @(region,state) hookeTensor( E(self.TR.pointLocation(region.x(:),region.y(:))) );
            
            specifyCoefficients(self.PDEmodel,'m',1,'d',0,'c',c,'a',0,'f',[0;0]);
            self.FEM = assembleFEMatrices(self.PDEmodel , 'nullspace');
            
        end
        %------------------------------------------------------------------
        %------------------------------------------------------------------
    end % endMethods
end % endClass






