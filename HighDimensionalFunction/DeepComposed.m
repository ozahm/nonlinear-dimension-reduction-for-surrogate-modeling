classdef DeepComposed < Function
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        
    end
    
    methods
        function u = DeepComposed(level)
            
            if nargin<1
                level=3;
            end
            
            u.evaluationAtMultiplePoints = true;
            u.dim = 2^level;
            u.outputSize = [1,1];
            
            %%%%%%%%%%%%%%%%%%%%%%%%
            
            
            X = cell(u.dim,1);
            for i=1:u.dim
                X{i}=UniformRandomVariable(-1,1);
            end
            X = RandomVector(X);
            RV = getStandardRandomVector(X);
            u.measure = ProductMeasure(RV);
            
            
        end
        
        function [y,g] = eval(u,x)
            
            dim = size(x,2);
            
            if dim==2
                xl = x(:,1);
                xr = x(:,2);
                [y,hl,hr] = u.h(xl,xr);
                g = [hl hr];
            else
                [xl,gl] = u.eval(x(:,1:dim/2));
                [xr,gr] = u.eval(x(:,dim/2+1:end));
                [y,hl,hr] = u.h(xl,xr);
                g = [gl.*hl gr.*hr];
            end
        end
        
        function [y,gt,gs] = h(u,t,s)
            
            y = (2+t.*s).^2/9;
            gt = 2*(2+t.*s)/9.*s;
            gs = 2*(2+t.*s)/9.*t;

%             y = (2+t.*abs(s)).^2/9;
%             gt = 2*(2+t.*abs(s))/9.*abs(s);
%             gs = 2*(2+t.*abs(s))/9.*t.*sign(s);

        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function g = evalGradient(u,x)
            [~,g] = u.eval(x);
        end
        
    end
    
end

