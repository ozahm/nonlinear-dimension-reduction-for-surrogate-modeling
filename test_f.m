%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Choose the function
u = BoreholeFunction();
% u = cosOfNorm(10);
RV = RandomVector(u.measure.measures);

%% Initialize J

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
J = CostFunction_FeatureMap(X,guX);

N_validation = 1000;
X_validation = RV.random(N_validation);
guX_validation = u.evalGradient(X_validation);
J_validation = CostFunction_FeatureMap(X_validation,guX_validation);

%% Compute g

m = 3;

opts = CostFunction_FeatureMap.OMP_default_opts();
opts.maxit = 5;

g = SmoothFeatureMap.randn(RV,m);
[g,J_iter,output] = J.OMP(g,opts);

% plotmatrix(g.eval(RV.random(10000)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check that it is (almost) always better to use the gradient

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
g = SmoothFeatureMap.randn(RV,m);
[g,J_iter,output] = J.OMP(g,opts);

% plotmatrix(g.eval(RV.random(10000)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check that it is (almost) always better to use the gradient

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
J_profile = CostFunction_Prof
J_profile = CostFunction_Profile(g,X,uX,guX);

N_validation = 1000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);
J_profile_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation);

f = SmoothProfile.randn(m);

%
clc

J_profile_l2 = J_profile;
J_profile_l2.with_derivative = 0;
J_profile_validation.with_derivative = 0;

[f,J] = J_profile.minimize(f);
[J] = J_profile_validation.eval(f);
disp('With gradient')
disp(J)

[f,J] = J_profile_l2.minimize(f);
[J] = J_profile_validation.eval(f);
disp('WithOUT gradient')
disp(J)

%% Try the OMP method

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);

% Create cost function
J_profile = CostFunction_Profile(g,X,uX,guX);
J_profile.with_derivative = 1;

opts = CostFunction_Profile.OMP_default_opts();
opts.maxit = 200;
opts.numberOfNewTermsPerIteration = 2;
f = SmoothProfile.randn(m);
[f,J_iter,output] = J_profile.OMP(f,opts);

% Validation
N_validation = 1000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);
J_profile_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation);
J_profile_validation.with_derivative = 1;

J_val = zeros(size(J_iter));
for i=1:length(J_iter)
    J_val(i) = J_profile_validation.eval(output.f{i});
end

semilogy(J_val)
hold on
semilogy(J_iter)
hold off
legend({'Validation','Training'})



