classdef SmoothProfile
    
    % Method to handel smooth polynomial profile function of the form
    %   f(z) = f(z_1,...z_m)
    %
    % where z = g(x) for some SmoothFeatureMap g such that 
    %   E[g(x)] = 0
    %   Cov[g(x)] = Id
    
    properties
        
        RV_normal % Random vector defining the measure on the input space
        m % dimension of the feature space
        
        F % FunctionalBasis
        coef % cardG-by-1 double containing the coefficients of the SmoothProfile
        grad_F % dim-by-1 cell of FunctionalBasis: grad_F{i} == \partial_i F
        reduced_margin % ?-by-dim integer containing the reduced margin of the downward closed set (=lower set) of F
    end
    
    methods
        %------------------------------------------------------------------
        function self = SmoothProfile(m,p)
            
            if ~isa(m,'double')
                error('Not implemented')
            end
            if nargin<2
                p = 0;
            end
            
            self.m = m;
            self.RV_normal = repmat({NormalRandomVariable(0,1)},m,1);
            self.RV_normal = RandomVector(self.RV_normal);
            
            % Initialize F
            max_degree_per_dim = 15;
            H = cellfun( @(X)PolynomialFunctionalBasis(X,0:max_degree_per_dim) , orthonormalPolynomials(self.RV_normal) ,'UniformOutput',0);
            H = FunctionalBases(H);
            I = MultiIndices.withBoundedNorm(self.m,1,p);
            self.F = SparseTensorProductFunctionalBasis(H,I);
            self.F.isOrthonormal = 0;
            
            % Initialize grad_F
            self.grad_F = cell(self.m,1);
            for i=1:self.m
                n = zeros(1,self.m);
                n(i) = 1;
                self.grad_F{i} = self.F.derivative(n);
            end
            
            % Initialize the rest
            self.coef = zeros(self.F.cardinal(),1);
            self.reduced_margin = self.F.indices.getReducedMargin();
            
        end
        %------------------------------------------------------------------
        function [fZ,grad_fZ,FZ,grad_FZ] = eval(self,Z)
            % function [fZ,grad_fZ,FZ,grad_FZ] = eval(self,Z)
            % Z: N-by-m double (sample)
            % fZ: N-by-1 double
            % grad_fX: N-by-m double
            % FZ: N-by-card(F) double (sample)
            % grad_FX: N-by-1 cell containing card(F)-by-m double
            
            FZ = self.F.eval(Z);
            fZ = FZ*self.coef;
            
            if nargout<4
                grad_fZ = cellfun( @(F)F.eval(Z)*self.coef, self.grad_F, 'UniformOutput', 0);
                grad_fZ = [grad_fZ{:}];
            else
                grad_FZ = cellfun( @(F)F.eval(Z), self.grad_F, 'UniformOutput', 0);
                grad_FZ = [grad_FZ{:}];
                grad_FZ = mat2cell(grad_FZ,ones(size(grad_FZ,1),1),size(grad_FZ,2));
                grad_FZ = cellfun(@(x)reshape(x,self.F.cardinal(),self.m),grad_FZ, 'UniformOutput', 0);
                grad_fZ = cellfun(@(FX) self.coef'*FX,grad_FZ, 'UniformOutput', 0);
                grad_fZ = vertcat(grad_fZ{:});
            end
        end
        %------------------------------------------------------------------
        function [fZ,grad_fZ,FZ,grad_FZ] = eval_on_reduced_margin(self,Z,I)
            % [fZ,grad_fZ,FZ,grad_FZ] = eval_on_reduced_margin(self,X,I)
            % X: N-by-dim double (sample)
            % I: MultiIndices
            % fZ: N-by-1 double
            % grad_fX: N-by-m double
            % FZ: N-by-card(F) double (sample)
            % grad_FX: N-by-1 cell containing card(F)-by-m double
            
            if nargin<3
                I = self.reduced_margin;
            end
            
            self.F.indices = I;
            for i=1:self.m
                self.grad_F{i}.indices = I;
            end
            self.coef = zeros(I.cardinal(),1);
            self.reduced_margin = [];
            [fZ,grad_fZ,FZ,grad_FZ] = self.eval(Z);
            
            
        end
        %------------------------------------------------------------------
        function [self,new_in_margin] = get_marginExtended_Profile(self,ind)
            %  function f = get_marginExtended_Profile(f,ind)
            % Extend the basis of f with its (zero) components located on 
            % the reduced margin of its lower-set. If "ind" is specified,
            % extend f with only the ind-th element of the reduced margin.
            %
            % Warning: the reduced maring is NOT updated, unless ind is
            % specified
            
            if nargin<2
                ind = 1:self.reduced_margin.cardinal();
            end
            I = self.F.indices;
            self.F.indices.array = [I.array ; self.reduced_margin.array(ind,:)];
            for i=1:self.m
                self.grad_F{i}.indices.array = self.F.indices.array;
            end
            self.coef = [self.coef ; zeros(length(ind),1)];
            
            % Update (or not) the reduced margin
            if nargin>=2
                [self.reduced_margin,~,new_in_margin] = updateReducedMargin(self.reduced_margin,I,ind);
            else
                self.reduced_margin = [];
            end
        end
        %------------------------------------------------------------------
        function gram = H1_gram_matrix(self)
            % Compute the H1-Gram matrix of F
            
            gram = speye(self.F.cardinal());
            gram = spdiags(sum(self.F.indices.array,2)+1,0,gram);
            
        end
        %------------------------------------------------------------------
    end
    methods (Static)
        %------------------------------------------------------------------
        function self = randn(m)
            self = SmoothProfile(m);
            self.coef = randn(size(self.coef));
        end
        %------------------------------------------------------------------
    end
end
