classdef SmoothFeatureMap
    
    % Method to handel smooth function of the form
    %   g(x) = (g_1(x),...,g_m(x))
    % where g_i are multivariate polynomial functions
    % 
    
    properties
        
        RV % Random vector defining the measure on the input space
        dim % input space dimension
        m % dimension of the feature space
        
        G % FunctionalBasis used for each of the components of g
        coef % cardG-by-m double containing the coefficients of each components of g
        grad_G % dim-by-1 cell of FunctionalBasis: grad_G{i} == \partial_i G
        reduced_margin % ?-by-dim integer containing the reduced margin of the downward closed set (=lower set) of G
    end
    
    methods
        %------------------------------------------------------------------
        function self = SmoothFeatureMap(RV,m,p)
            
            if nargin<3
                p=1;
            end
            
            self.RV = RV;
            self.m = m;
            self.dim = size(RV.random(1),2);
            
            % Initialize G
            max_degree_per_dim = 20;
            H = cellfun( @(X)PolynomialFunctionalBasis(X,0:max_degree_per_dim) , orthonormalPolynomials(self.RV) ,'UniformOutput',0);
            H = FunctionalBases(H);
            I = MultiIndices.withBoundedNorm(self.dim,1,p);
            self.G = SparseTensorProductFunctionalBasis(H,I);
            
            % Initialize everything
            self.reduced_margin = self.G.indices.getReducedMargin();%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            self.coef = zeros(self.G.cardinal(),self.m);
            self.grad_G = cell(self.dim,1);
            for i=1:self.dim
                n = zeros(1,self.dim);
                n(i) = 1;
                self.grad_G{i} = self.G.derivative(n);
            end
            
        end
        %------------------------------------------------------------------
        function [gX,grad_gX,grad_GX] = eval(self,X)
            % [gX,grad_gX,grad_GX] = eval(self,X)
            % X: N-by-dim double (sample)
            % gX: N-by-m double
            % grad_gX: N-by-1 cell containing dim-by-m double
            % grad_GX: N-by-1 cell containing cardG-by-m double
            
            gX = self.G.eval(X)*self.coef;
            grad_GX = cellfun( @(G)G.eval(X), self.grad_G, 'UniformOutput', 0);
            grad_GX = [grad_GX{:}];
            grad_GX = mat2cell(grad_GX,ones(size(grad_GX,1),1),size(grad_GX,2));
            grad_GX = cellfun(@(x)reshape(x,self.G.cardinal(),self.dim),grad_GX, 'UniformOutput', 0);
            grad_gX = cellfun(@(GX) GX'*self.coef,grad_GX, 'UniformOutput', 0);
        end
        %------------------------------------------------------------------
        function [gX,grad_gX,grad_GX] = eval_on_reduced_margin(self,X,I)
            % [gX,grad_gX,grad_GX] = eval_on_reduced_margin(self,X)
            % X: N-by-dim double (sample)
            % I: MultiIndices
            % gX: N-by-m double
            % grad_gX: N-by-1 cell containing dim-by-m double
            % grad_GX: N-by-1 cell containing cardG-by-m double
            
            if nargin<3
                I = self.reduced_margin;
            end
            
            self.G.indices = I;
            for i=1:self.dim
                self.grad_G{i}.indices = I;
            end
            self.coef = zeros(I.cardinal(),self.m);
            self.reduced_margin = [];
            [gX,grad_gX,grad_GX] = self.eval(X);
            
        end
        %------------------------------------------------------------------
        function self = standardize_pushforward(self)
            % Transform coef so that g becomes a zero-mean randon vector with
            % identity covariance matrix
            
            % Compute the mean and covariance of g
            mu = zeros(1,self.m);
            cov = zeros(self.m);
            for i=1:self.m
                
                gi = FunctionalBasisArray(self.coef(:,i),self.G);
                mu(i) = mean(gi);
                for j=1:i
                    gj = FunctionalBasisArray(self.coef(:,j),self.G);
                    cov(j,i) = dotProductExpectation( gi,gj ) - mu(i)*mu(j);
                    cov(i,j) = cov(j,i);
                end
            end
            
            % Remove the mean and unitize the covariance
            self.coef(1,:) = self.coef(1,:)-mu;
            self.coef = (self.coef)/chol(cov+eye(size(cov))*norm(cov)*1e-8);
        end
        %------------------------------------------------------------------
        function [self,new_in_margin] = get_marginExtended_FeatureMap(self,ind)
            % function g = get_marginal_FeatureMap(g,ind)
            % Extend the basis of g with its (zero) components of g located 
            % on the reduced margin of its lower-set. If "ind" is specified,
            % extend g with only the ind-th element of the reduced margin.
            %
            % Warning: the reduced maring is NOT updated, unless ind is
            % specified
            
            if nargin<2
                ind = 1:self.reduced_margin.cardinal();
            end
            I = self.G.indices;
            self.G.indices.array = [I.array ; self.reduced_margin.array(ind,:)];
            for i=1:self.dim
                self.grad_G{i}.indices.array = self.G.indices.array;
            end
            self.coef = [self.coef ; zeros(length(ind),self.m)];
            
            % Update (or not) the reduced margin
            if nargin>=2
                [self.reduced_margin,~,new_in_margin] = updateReducedMargin(self.reduced_margin,I,ind);
            else
                self.reduced_margin = [];
            end
        end
        %------------------------------------------------------------------
        function self = set_coef(self,coef)
            self.coef = reshape(coef,size(self.coef));
        end
        %------------------------------------------------------------------
    end
    methods (Static)
        %------------------------------------------------------------------
        function self = randn(RV,m)
            self = SmoothFeatureMap(RV,m);
            self.coef = randn(size(self.coef));
        end
        %------------------------------------------------------------------
        function self = identity(RV)
            m = size(RV.random(1),2);
            self = SmoothFeatureMap(RV,m);
            self.coef = self.G.indices.array;
        end
        %------------------------------------------------------------------
    end
    
end