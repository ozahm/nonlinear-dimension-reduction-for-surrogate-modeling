classdef (Abstract) Measure
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
    end
    
    methods (Abstract)
        ok = eq(p,q)
        % function ok = eq(p,q)
        % Checks if the two Measure p and q are identical
        % f: Measure
        % g: Measure
        % ok: boolean

        m = mass(p)
        % function m = mass(p)
        % Returns the mass of the Measure p

        s = support(p)
        % function s = support(p)
        % Returns the support of the Measure p

        s = ndims(p)
        % function s = ndims(p)
    end
    
    
end
