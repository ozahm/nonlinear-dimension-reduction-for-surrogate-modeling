classdef LebesgueMeasure < Measure
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        s
    end
    
    methods
        function l = LebesgueMeasure(support)
            l.s = support;
        end
        
        function n = ndims(~)
            n = 1;
        end
        
        function ok = eq(m1,m2)
            if ~(isa(r1,'LebesgueMeasure') && isa(m2,'LebesgueMeasure') )
                ok = 0;
            elseif ~strcmp(class(m1),class(m2))
                ok = 0;
            else
                ok = all(support(m1) == support(m2));
            end
        end
        
        function m = mass(l)
            m = l.s(2) - l.s(1);
        end
        
        function s = support(l)
            s = l.s;
        end

    end
    
    
end
