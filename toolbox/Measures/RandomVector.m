classdef RandomVector < ProbabilityMeasure
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        randomVariables
        copula = IndependentCopula()
    end
    
    methods
        function rv = RandomVector(varargin)
            % function rv = RandomVector(r,d) or rv =
            % RandomVector({r1,r2,...}) or rv = RandomVector(r1,r2,...)
            % Constructor for the RandomVector class, takes as inputs a
            % cell of RandomVariable, several RandomVariable or a
            % RandomVariable and the dimension of the RandomVector
            % The random variables are assumed independent and non
            % conditional
            % varargin: r, r1, r2, ...: RandomVariable, d: 1-by-1 double
            % rv: RandomVector
            
            if nargin==1 && isa(varargin{1}, 'RandomVector')
                rv = varargin{1};
            elseif nargin==1 && isa(varargin{1}, 'RandomVector') && isa(varargin{2}, 'Copula')
                rv = varargin{1};
                rv.copula = varargin{2};
            elseif nargin == 1 && isa(varargin{1}, 'RandomVariable')
                rv.randomVariables{1} = varargin{1};
            elseif nargin == 1 && isa(varargin{1},'cell')
                rv.randomVariables = varargin{1};
            elseif nargin == 2 && isa(varargin{1},'RandomVariable') && isa(varargin{2}, 'double')
                rv.randomVariables = cell(1,varargin{2});
                for i = 1:varargin{2}
                    rv.randomVariables{i} = varargin{1};
                end
            else
                rv.randomVariables = cell(1,nargin);
                for i = 1:nargin
                    rv.randomVariables{i} = varargin{i};
                end
            end
        end
        
        function n = ndims(rv)
            n = sum(cellfun(@ndims,rv.randomVariables));
        end
        
        function ok = eq(r1,r2)
            % function ok = eq(r1,r2)
            % Checks if two RandomVector r1 and r2 are equal
            % r1: RandomVector
            % r2: RandomVector
            % ok: boolean
            
            if ~(isa(r1,'RandomVector') && isa(r2,'RandomVector'))
                ok = 0;
            elseif length(r1.randomVariables) ~= length(r2.randomVariables)
                ok = 0;
            else
                ok = 1;
                for i = 1:length(r1.randomVariables)
                    ok = ok & (r1.randomVariables{i} == r2.randomVariables{i});
                end
            end
        end
        
        function Y = marginal(X,ind)
             if ~isa(X.copula,'IndependentCopula')
                error('only works for independent copula');                
             end
            Y = RandomVector(X.randomVariables(ind));
        end
        
        function ok = ne(r1,r2)
            % function ok = ne(r1,r2)
            % Checks if two RandomVector r1 and r2 are not equal
            % r1: RandomVector
            % r2: RandomVector
            % ok: boolean
            
            ok = ~eq(r1,r2);
        end      
        
        function rvstd = getStandardRandomVector(rv)
            % function rvstd = getStandardRandomVector(rv)
            % Returns the standard random vector of rv
            % rv: RandomVector
            % rvstd: RandomVector
            
            Xstd = cell(1,numel(rv));
            for k=1:numel(rv)
                Xstd{k} = getStandardRandomVariable(rv.randomVariables{k});
            end
            rvstd = RandomVector(Xstd);
        end
        
        function G = isoProbabilisticGrid(rv,n)
            % function G = isoProbabilisticGrid(rv,n)
            % Generates a grid of (n(1)-1)x...x(n(d)-1)
            % points (x_{i_1}^1,...x_{i_d}^d) such that 
            % the N = (n(1))x...x(n(d)) sets [x_{i_1-1}^1,x_{i_1}^1]x ... x[x_{i_d-1}^1,x_{i_d}^1]
            % have the same probability p = 1/N.
            % rv: RandomVector of ndims d, with IndependentCopula
            % n = 1-by-d integer
            % G: FullTensorGrid
            %
            % function G = isoProbabilisticGrid(rv,p)
            % Specify the probability of each set
            % p : 1-by-1 double (<1)
            
            if ~isa(rv.copula,'IndependentCopula')
                error('only works for independent copula')
            end
            
            d= numel(rv);
            
            if numel(n)==1 && n<1
                p=n;
                n=ceil(p^(-1/d));
            end
            
            if numel(n)~=d 
                n = repmat(n,1,d); 
            end

            b = cell(1,d);            
            for k =1:d
                b{k} = isoProbabilisticGrid(rv.randomVariables{k},n(k));
            end
            G = FullTensorGrid(b);
        end
        
        function A = lhsRandom(rv,n)
            % function A = lhsRandom(X,n,p)
            % Latin Hypercube Sampling of the RandomVector X of n points
            % X: RandomVector
            % n: integer
            % A: 1-by-numel(rv) cell containing n-by-1 doubles
            
            A = lhsdesign(n,numel(rv));
            A = transfer(RandomVector(UniformRandomVariable(0,1),numel(rv)),rv,A);
            A = mat2cell(A,n,ones(1,numel(rv)));
        end
        
        function m = mean(rv)
            % function m = mean(X)
            % Computes the means of the RandomVector X
            % X: RandomVector
            % m: 1-by-1 double
            
            m = cell(1,numel(rv));
            for i = 1:numel(rv)
                m{i} = mean(rv.randomVariables{i});
            end
        end
        
        function n = numel(rv)
            % function n =  numel(rv)
            % Gets the number of RandomVariable in the RandomVector
            % rv: RandomVector
            % n: integer
            
            n = length(rv.randomVariables);
        end
        
        function p = pdf(rv,x)
            % function px = pdf(rv,x)
            % Computes the probability density function of each
            % RandomVariable in rv at points x, x must have ndims(rv)
            % columns
            % rv: RandomVector
            % x: 1-by-n or n-by-1 double
            % p: 1-by-n or n-by-1 double
            
            
            p = zeros(size(x));
            for i = 1:numel(rv)
                p(:,i) = pdf(rv.randomVariables{i},x(:,i));
            end
            p = prod(p,2);
            
            if ~isa(rv.copula,'IndependentCopula')
                u = zeros(size(x));
                for i = 1:numel(rv)
                    u(:,i) = cdf(rv.randomVariables{i},x(:,i));
                end
                p = p.*pdf(rv.copula,u);
            end
            
        end
        
        function u = cdf(rv,x)
            % function Fx = cdf(rv,x)
            % Computes the cumulative distribution function
            % at points x, x must have ndims(rv)
            % columns
            % rv: RandomVector
            % x: 1-by-n or n-by-1 double
            % Fx: 1-by-n or n-by-1 double
            
            u = zeros(size(x));
            for i = 1:numel(rv)
                u(:,i) = cdf(rv.randomVariables{i},x(:,i));
            end
            u = cdf(rv.copula,u);
            
        end
        
        
        function r = random(rv,n,varargin)
            % function r = random(X,n) 
            % Generates n random numbers according to the distributions of
            % RandomVariable in the RandomVector rv
            % X: RandomVector
            % n: integer
            % r: n-by-numel(rv) double
            
            if nargin > 2
                warning('random should have only two input arguments.')
            end
            
            if nargin==1
                n=1;
            end
            
            if numel(n)>1
                error('n must be an integer')
            end
            
            dims =  cellfun(@ndims, rv.randomVariables);
            r = zeros(n, sum(dims));
            for i = 1:length(rv.randomVariables)
                rep = sum(dims(1:i-1))+(1:dims(i));
                r(:,rep) = random(rv.randomVariables{i},n);
            end
        end
        
        function s = std(rv)
            % function s = std(X)
            % Computes the standard deviation of the RandomVariable in the
            % RandomVector rv
            % X: RandomVector
            % s: 1-by-numel(rv) cell
            
            s = cell(1,numel(rv));
            for i = 1:numel(rv)
                s{i} = std(rv.randomVariables{i});
            end
        end
        
        function y = transfer(X,Y,x)
            % function y = transfer(X,Y,x)
            % Transfers from the RandomVector X to the RandomVector Y, at
            % points x
            % X: RandomVariable
            % Y: RandomVariable
            % x: 1-by-numel(X) or numel(X)-by-1
            % y: 1-by-numel(X) or numel(X)-by-1
            
            if isa(x,'cell')
                x = [x{:}];
            end
            
            if isa(Y,'RandomVariable')
                Y = RandomVector(Y,numel(X));
            end
            
            if numel(X) ~= numel(Y)
                error('The two RandomVector must have the same dimension');
            end
            
            y = zeros(size(x,1),numel(Y));
            
            for i = 1:numel(X)
                y(:,i) = transfer(X.randomVariables{i},Y.randomVariables{i},x(:,i));
            end
        end
        
        function v = variance(rv)
            % function v = variance(X)
            % Computes the variance of the RandomVariable in the
            % RandomVector rv
            % X: RandomVector
            % s: 1-by-numel(rv) cell
            
            v = cell(1,numel(rv));
            for i = 1:numel(rv)
                v{i} = variance(rv.randomVariables{i});
            end
        end
        
        
        function s = support(rv)
            % function s = support(rv)
            % s : cell array containing the supports of random variables
            if ~isa(rv.copula,'IndependentCopula')
                error('Not implemented for non IndenpendentCopula.')
            end
            s=cell(1,numel(rv));
            for k=1:numel(rv)
                s{k} = support(rv.randomVariables{k});
            end
        end
        
        function s = truncatedSupport(rv)
            % function s = truncatedSupport(rv)
            % s : cell array containing the truncated supports of random variables
            if ~isa(rv.copula,'IndependentCopula')
                error('Not implemented for non IndenpendentCopula.')
            end
            s=cell(1,numel(rv));
            for k=1:numel(rv)
                s{k} = truncatedSupport(rv.randomVariables{k});
            end
        end
        
        
        function p = orthonormalPolynomials(rv,n)
            p = cell(1,numel(rv));
            if nargin==2 && numel(n)==1
                n = repmat(n,1,numel(rv));
            end
            for k=1:numel(rv)
                if nargin==1
                    p{k} = orthonormalPolynomials(rv.randomVariables{k});
                else
                    p{k} = orthonormalPolynomials(rv.randomVariables{k},n(k));
                end
            end
        end
        
        function X = permute(X,p)
           % function X = permute(X,p)
           % X : randomVector
           % p : array containing a permutation of 1:numel(X)
           X.randomVariables = X.randomVariables(p);
        end
        
        function sz = size(X)
            sz = [numel(X),1];
        end
        
    end
    
    methods (Static, Hidden)
        function initstate()
            % function initstate()
            % Changes the state of the pseudo-random numbers generators
            
            pause(eps)
            rng(sum(100*clock));
        end
        

    end
    
    
    
end
