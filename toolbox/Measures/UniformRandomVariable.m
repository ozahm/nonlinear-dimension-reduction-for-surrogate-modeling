classdef UniformRandomVariable < RandomVariable
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        a
        b
    end
    
    methods
        
        function  X = UniformRandomVariable(a,b)
            % function  X = UniformRandomVariable(a,b)
            % Uniform random variable on [a,b] (by default [-1,1])
            % a: 1-by-1 double (optional)
            % b: 1-by-1 double (optional)
            % X: UniformRandomVariable with parameters a and b
            
            X@RandomVariable('unif');
            
            if nargin==0
                X.a = -1;
                X.b = 1;
            else
                X.a = a;
                X.b = b;
            end
            
        end
        
        function X = shift(X,b,s)
            
            X.a = s*X.a+b;
            X.b = s*X.b+b;
                 
        end
        
        function Xstd = getStandardRandomVariable(X)
            % function Xstd = getStandardRandomVariable(X)
            % Returns the standard uniform random variable on [-1,1]
            % X: UniformRandomVariable
            % Xstd: UniformRandomVariable
            
            Xstd = UniformRandomVariable();
        end
        
        function s = support(X)
            % function s = support(X)
            % Returns the support of the uniform random variable X
            % X: UniformRandomVariable
            % s: 1-by-2 double
            
            s = [X.a,X.b];
        end
        
        function p = orthonormalPolynomials(X,varargin)
            % function p = orthonormalPolynomials(X,n)
            % Returns the n first orthonormal polynomials according to
            % the UniformRandomVariable X
            % X: UniformRandomVariable
            % n: integer (optional)
            % p: LegendrePolynomials
            
                p = LegendrePolynomials(varargin{:});
                if X~=UniformRandomVariable(-1,1)
%                    warning('ShiftedOrthonormalPolynomials are created')
                    p = ShiftedOrthonormalPolynomials(p,(X.a+X.b)/2,(X.b-X.a)/2);
                end
                
                
        end
        
        function p = getParameters(X)
            % function p = getParameters(X)
            % Returns the parameters of the uniform random variable X in an
            % array
            % X: UniformRandomVariable
            % p: 1-by-2 cell
            
            p = {X.a,X.b};
        end
        
        function [m,v] = randomVariableStatistics(X)
            % function [m,v] = randomVariableStatistics(X)
            % Computes the mean m and the variance v of the uniform random
            % variable X
            % X: UniformRandomVariable
            % m: 1-by-1 double
            % v: 1-by-1 double
            
            [m,v] = unifstat(X.a,X.b);
        end
    end
    
end