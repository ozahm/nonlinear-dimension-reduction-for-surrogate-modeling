classdef IndependentCopula
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
    end
    
    methods
        
        function C = IndependentCopula()
            
        end
        
        function p = pdf(~,u)
            p = ones(size(u,1),1);
        end
        
        function c = cdf(~,u)
            c = prod(u,2);
        end
               
    end
end