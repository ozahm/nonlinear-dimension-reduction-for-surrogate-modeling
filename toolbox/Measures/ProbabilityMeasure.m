classdef ProbabilityMeasure < Measure
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        
    end
    
    methods
        
        function m = mass(p)
            m = 1;
        end
        

        function x = randomRejection(X,n,Y,c,m)
            % function x = randomRejection(X,n,Y,c)
            % Generates n samples of X with a rejection method, with
            % rejection probability measure Y 
            % X and Y should admit densities with respect to the same reference 
            % measure, denoted p and q respectively, and such that 
            % p(x) <= c g(x) for some constant c
            % X : ProbabilityMeasure
            % n : integer number of samples
            % c : real number 
            % m : integer (number of simultaneous trials, n by default)
            % if c is not provided or empty, c is taken as (an upper bound of) an
            % approximation sup_x 
            % f(x)/g(x), where the supremum is taken over the support of $Y$
            
            if nargin <= 3 || isempty(c)
                x = random(Y,100000);
                c = 1.1 * max(X.pdf(x)/Y.pdf(x)); 
            end
            
            if nargin <= 4
                m=n;
            end
            
            x = zeros(0,ndims(X));
            while length(x)<n
                y = random(Y,m);
                u = rand(m,1);
                t = u.*(c*Y.pdf(y))<X.pdf(y);
                x = [x;y(t,:)];
            end
            x = x(1:n,:);
        end

        
    end
    
    
end
