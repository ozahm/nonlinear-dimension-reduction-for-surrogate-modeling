classdef PolynomialFunctionalBasis < FunctionalBasis
    % Class PolynomialFunctionalBasis
              
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        basis
        indices
    end
    
    methods
        function p = PolynomialFunctionalBasis(basis,indices)
            % function p = PolynomialFunctionalBasis(basis,indices)
            % basis : UnivariatePolynomials
            % indices : array containing the selected polynomials indices
            % p: PolynomialFunctionalBasis
            
            p.basis = basis;
            p.indices = indices;
            p.measure = basis.measure;

            if isa(basis,'OrthonormalPolynomials') || isa(basis,'ShiftedOrthonormalPolynomials')
                p.isOrthonormal = true;
            end
        end
        
        function ok = eq(p,q)
            % function ok = eq(p,q)
            % Checks if the two objects p and q are identical
            % p: PolynomialFunctionalBasis
            % q: PolynomialFunctionalBasis
            % ok: boolean
            
            if ~isa(q,'PolynomialFunctionalBasis')
                ok = 0;
            else
                try
                    ok = all(p.basis == q.basis) & all(p.indices == q.indices);
                catch
                    ok = 0;
                end
            end
        end
        
        function n = cardinal(p)
            n = numel(p.indices);
        end
        

        function px = eval(p,x)
            % function px = eval(p,x)
            % Computes evaluations of the polynomials of p.basis of degrees
            % in p.indices at points x
            % p: PolynomialFunctionalBasis
            % x: n-by-1 or 1-by-n double
            % px: n-by-d double, where d = size(p.indices,1)
            
            px = polyval(p.basis,p.indices,x);
        end
        
        function h = kron(p,q)    
            % See FunctionalBasis/kron
            m = max(p.indices)+max(q.indices); 
            b = PolynomialFunctionalBasis(p.basis,0:m);
            b.measure = p.measure;
            fun = @(x) reshape(repmat(eval(p,x),[1,1,cardinal(q)]).*...
                               repmat(permute(eval(q,x),[1,3,2]),[1,cardinal(p),1]),...
                               [size(x,1) , cardinal(p)*cardinal(q)]);
            rv = b.measure;
            Q = gaussIntegrationRule(rv,2*m);
            a = b.projection(fun,Q);  
            h = SubFunctionalBasis(b,a.data);
        end

        function f = derivative(h,k,rv)
            % Computes the k-th order derivative of the functions of the
            % basis h projected on h
            % h: PolynomialFunctionalBasis
            % k: integer
            % rv: Measure, optional if an h.basis is an
            % OrthonormalPolynomials
            % f: SubFunctionalBasis
            
            if nargin < 3
                if isempty(h.measure)
                    error('Must specify a Measure')
                else
                    rv = h.measure;
                end
            end
            
            m = ceil(max(h.indices) - (k-1)/2);
            I = gaussIntegrationRule(rv,m);
            f = h.projection(@(x) dnPolyval(h.basis,k,h.indices,x),I);
            
            f = subFunctionalBasis(f);
        end
        
        function g = gradient(h,rv)
            % Computes the first order derivative of the functions of the
            % basis h projected on h
            % h: PolynomialFunctionalBasis
            % rv: Measure, optional if an h.basis is an
            % OrthonormalPolynomials
            % g: SubFunctionalBasis
            
            g = derivative(h,1,rv);
        end
        
        function H = hessian(h,rv)
            % Computes the second order derivative of the functions of the
            % basis h projected on h
            % h: PolynomialFunctionalBasis
            % rv: Measure, optional if an h.basis is an
            % OrthonormalPolynomials
            % H: SubFunctionalBasis
            
            H = derivative(h,2,rv);
        end
        
        function y = evalDerivative(h,k,x)
            % function y = evalDerivative(h,k,x)
            % Evaluates the k-th order derivative of the functions of the
            % basis h at points x
            % h: PolynomialFunctionalBasis
            % k: integer
            % x: N-by-1 or 1-by-N array of doubles
            % y: N-by-numel(h) array of doubles
            
            y = dnPolyval(h.basis,k,h.indices,x);
        end
        
        function M = gramMatrix(f,rv)
            % function M = gramMatrix(f,rv)
            % Computes the Gram matrix of the basis f. The Gram
            % matrix is the matrix of the dot products between each
            % possible couple of basis functions of f. The dot product in
            % the dimension i is computed according to Measure rv
            % if provided, or according to the Measure in f if not.
            % f: PolynomialFunctionalBasis
            % rv: Measure (optional)
            % M: P-by-P double, where P is the number of multi-indices
            
            if nargin < 2
                if isempty(f.measure)
                    error('Must specify a Measure')
                else
                    rv = f.measure;
                end
            end
            
            if ismethod(f.basis,'moment')
                ind = f.indices;
                list = [repmat(ind',length(ind),1), reshape(repmat(ind,length(ind),1),numel(ind)^2,1)];
                M = reshape(moment(f.basis,list,rv),[length(ind) length(ind)]);
            else
                error('Not implemented');
            end
        end
        
        function H = one(p)
            [c,I] = one(p.basis);
            [ok,rep] = ismember(I,p.indices);
            if ~all(ok)
                error('constant 1 can not be represented')
            end
            H = zeros(numel(p.indices),1);
            H(rep) = c;
        end
        
        function m = mean(p,varargin)
            % function m = mean(f,rv)
            % Gives the expectation of basis functions, according to the
            % randomVariable property of p if rv is not provided, or to the
            % RandomVariable rv if it is provided
            % rv : RandomVariable (optional)
            % m: n-by-1 double, where n is the length of the indices
            % property of p
            
            m = mean(p.basis,p.indices,varargin{:});
        end

        function rv = randomVariable(p)
            % function rv = randomVariable(p)
            % Returns the RandomVariable associated to p if it exists
            % p: PolynomialFunctionalBasis
            % rv: RandomVariable or empty array
            
            if isprop(p.basis,'randomVariable')
                rv = p.basis.randomVariable;
            else
                warning('Empty random variable.')
                rv = [];
            end
            
        end
        
        function nu = optimalSamplingMeasure(f)
            % function nu = optimalSamplingMeasure(f)
            % nu is the measure with randon derivative w.r.t measure f.measure
            % equal to the christoffel function of f
            % f : PolynomialFunctionalBasis
            % mu : ProbabilityMeasureWithRadonDerivative
            % 
            w = christoffel(f)/cardinal(f);
            nu = ProbabilityMeasureWithRadonDerivative(f.measure,w);

        end
              
        function x = interpolationPoints(h,x)
            % function x = interpolationPoints(h)
            % set of Chebyschev Points if h.indices is downward closed (0:p)
            % or magicPoints selected in a set of ChebychevPoints if not.
            
            if cardinal(h)==max(h.indices)+1
                x = chebychevPoints(cardinal(h));
            else
                if nargin==1
                    x = chebychevPoints(cardinal(h)*10);
                end
                x = magicPoints(h,x);
            end
        end
        
        function ch = christoffel(f,x)
            % function ch = christoffel(f)
            % or
            % function ch = christoffel(f,x)
            if ~f.isOrthonormal
                f=orthonormalize(f);
            end
            if nargin==1
                m = 2*max(f.indices);
                b = PolynomialFunctionalBasis(f.basis,0:m);
                b.measure = f.measure;
                fun = @(x) sum(eval(f,x).^2,2);
                Q = gaussIntegrationRule(b.measure,2*m);
                ch = b.projection(fun,Q);
            else
                ch = sum(abs(f.eval(x)).^2,2);
            end
            
        end
        
        function s = domain(p)
            s = domain(p.basis);
        end
        
        function n = ndims(p)
            n = ndims(p.basis);
        end
        
    end
    
end
