classdef FunctionalBasisArray < Function
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        data
        basis
        sz
    end
    
    methods
        function x = FunctionalBasisArray(data,basis,sz)
            % Class FunctionalBasisArray
            %
            % function x = FunctionalBasisArray(data,basis)
            % Function with value in R
            % f: FunctionalBasisArray
            % basis: FunctionalBasis
            % data: array of size [cardinal(basis),1]
            % containing the coefficents of the function on the basis
            %
            % x = FunctionalBasisArray(data,basis,sz)
            % Function with value in R^(sz(1)xsz(2)x...)
            % data: array of size [cardinal(basis),sz] containing the
            % coefficents of the function on the basis
            
            if nargin==0
                data=[];
                basis=[];
                sz=[];
            elseif nargin<=2
                sz=[1,1];
            end
            x.data = data(:);
            x.basis = basis;
            x.sz = sz;
            x.data = reshape(x.data,[cardinal(basis),sz]);
            
        end
          
        function f = plus(f,g)
            % function f = plus(f,g)
            % f: FunctionalBasisArray
            % g: FunctionalBasisArray
            
            f.data = f.data + g.data;
        end
        
        function f = uminus(f)
            % function f = uminus(f)
            % f: FunctionalBasisArray
            
            f.data = - f.data ;
        end
        
        function f = minus(f,g)
            % function f = minus(f,g)
            % f: FunctionalBasisArray
            % g: FunctionalBasisArray
            
            f = plus(f,-g);
        end
        
        function f = times(f,v)
            % function f = times(f,v)
            % f: FunctionalBasisArray
            % v: FunctionalBasisArray or double
            
            if isa(f,'FunctionalBasisArray') && isa(v,'FunctionalBasisArray')
                f.data = f.data .* v.data;
            elseif isa(f,'FunctionalBasisArray') && isa(v,'double')
                f.data = f.data * v;
                s = size(f.data);
                f.sz = s(2:end);
            elseif isa(f,'double') && isa(v,'FunctionalBasisArray')
                f = times(v,f);
            else
                error('Not implemented')
            end
        end
        
        function f = mtimes(f,v)
            % function f = mtimes(f,v)
            % Multiplies the double f.data by v
            % f: FunctionalBasisArray
            % v: double
            
            if isa(f,'FunctionalBasisArray') && isa(v,'double')
                f.data = f.data * v;
                s = size(f.data);
                f.sz = s(2:end);
            elseif isa(f,'double') && isa(v,'FunctionalBasisArray')
                f = mtimes(v,f);
            else
                error('Not implemented')
            end
        end
        
        function f = mrdivide(f,v)
            % function f = mrdivide(f,v)
            % Right divides the double f.data by v
            % f: FunctionalBasisArray
            % v: double
            
            if isa(f,'FunctionalBasisArray') && isa(v,'double')
                f.data = f.data / v;
            else
                error('Not implemented')
            end
        end
        
        
        function c = dot(f,g,varargin)
            % function c = dot(f,g)
            % Computes the dot product between the arrays f.data and g.data
            % treated as collections of vectors. The function calculates
            % the dot product of corresponding vectors along the first
            % array dimension whose size does not equal 1.
            %
            % function c = dot(f,g,dim)
            % Evaluates the scalar dot product of f.data and g.data along dimension dim
            % f: FunctionalBasisArray
            % g: FunctionalBasisArray
            % dim: positive integer scalar (optional)
            % c: 1-by-s double, where s is the size of f
            
            c = dot(f.data,g.data,varargin{:});
        end
        
        
        function n = norm(f,varargin)
            % function n = norm(f,p)
            % Computes the p-norm of the array f.data
            % f: FunctionalBasisArray
            % p: positive integer scalar or Inf or -Inf or 'fro' (optional), 'fro' by default
            % n: 1-by-1 double
            
            if nargin == 1
                n = norm(f,'fro');
            else
                n = norm(f.data(:,:),varargin{:});
            end
            
        end
        
        function x = israndom(~)
            % function x = israndom(f)
            % Determines if input f is random
            % f: FunctionalBasisArray
            % x: boolean
            
            x = 1;
        end
        
        function m = mean(f,varargin)
            % function m = mean(f,rv)
            % Computes the expectation of f, according to the measure
            % associated with the RandomVector rv if provided, or to the
            % standard RandomVector associated with each polynomial if not
            % f: FunctionalBasisArray
            % rv: RandomVector or RandomVariable (optional)
            % m: 1-by-s double, where s is the size of f
            
            M = mean(f.basis,varargin{:});
            m = dot(f.data,repmat(M,[1,f.sz]),1);
        end
        
        function m = expectation(f,varargin)
            % function m = expectation(f)
            % Computes the expectation of f
            % f: FunctionalBasisArray
            % See FunctionalBasisArray/mean
            m = mean(f,varargin{:});
        end
        
        function v = variance(f,varargin)
            % function v = variance(f,X)
            % Computes the variance of the random variable f(X).
            % If X is not provided, uses
            % random variables associated with underlying basis.
            % f: FunctionalBasisArray
            % X: RandomVector (optional)
            % v: 1-by-si double, where si is the size of f
            
            m = expectation(f,varargin{:});
            v = dotProductExpectation(f,f,[],varargin{:});
            v = v - m.^2;
        end
        
        function s = std(f,varargin)
            % function s = std(f,X)
            % Computes the standard deviation of the random variable f(X).
            % If X is not provided, uses
            % random variables associated with underlying basis.
            % f: FunctionalBasisArray
            % X: RandomVector (optional)
            % s: 1-by-si double, where si is the size of f
            
            
            s = sqrt(variance(f,varargin{:}));
        end
        
        function c = dotProductExpectation(f,g,dims,varargin)
            % function c = dotProductExpectation(f,g)
            % Computes the expectation of f(X)g(X) where X is the random
            % vector associated with underlying basis.
            %
            % function c = dotProductExpectation(f,g,[],X)
            % Computes the expectation of f(X)g(X) for the provided
            % RandomVector X
            %
            % function c = dotProductExpectation(f,g,Xdims,X)
            % For vector-valued functions of X, specify
            % the dimensions of f and g corresponding to RandomVector X
            %
            % f,g: FunctionalBasisArray
            % Xdims: D-by-1 or 1-by-D double (optional)
            % X: RandomVector (optional)
            % c: 1-by-s double, where s is the size of f
            
            
            if nargin == 2 || isempty(dims)
                dims = 1:length(f.basis);
            end
            
            if ~(f.basis == g.basis) || ~f.basis.isOrthonormal
                error('Not implemented');
            end
            
            c = dot(f,g,1);
        end
        
        function n = normExpectation(f,varargin)
            % function n = normExpectation(f,X)
            % Computes the L^2 norm of f(X).
            % If X is not provided, uses
            % random variables associated with underlying basis of f.
            % f: FunctionalBasisArray
            % rv: RandomVector (optional)
            % n: 1-by-s double, where s is the size of f
            
            n = sqrt(dotProductExpectation(f,f,[],varargin{:}));
        end
        
        function f = conditionalExpectation(f,dims,varargin)
            % function y = conditionalExpectation(f,dims,XdimsC)
            % Computes the conditional expectation of f with respect to
            % the random variables dims (a subset of 1:d). The expectation
            % with respect to other variables (in the complementary set of
            % dims) is taken with respect the probability measure given by RandomVector XdimsC
            % if provided, or with respect the probability measure
            % associated with the corresponding bases of f.
            % f: FunctionalBasisArray
            % dims: D-by-1 or 1-by-D double 
            %   or 1-by-d logical
            % XdimsC: RandomVector containing (d-D) RandomVariable (optional)
            
            h = conditionalExpectation(f.basis,dims,varargin{:});
            f.data = h.data * f.data(:,:);
            f.data = reshape(f.data,[size(f.data,1),f.sz]);
            f.basis = h.basis;
            return
            

        end
        
        function v = varianceConditionalExpectation(f,alpha)
            % function v = varianceConditionalExpectation(f,alpha)
            % Computes the variance of the conditional expectation of f in dimensions in dims
            % f: FunctionalBasisArray
            % alpha: n-by-D double, where D is equal to the number of random variables
            %    or n-by-d logical
            % v: n-by-1 double
            
            m = expectation(f);
            
            v = zeros(size(alpha,1),prod(f.sz));
            
            for i = 1:size(alpha,1)
                u = alpha(i,:);
                if isa(u,'logical')
                    u = find(u);
                end        
                if isempty(u)
                    v(i,:)=0;
                else
                    mi = conditionalExpectation(f,u);
                    vi = dotProductExpectation(mi,mi) - m.^2;
                    v(i,:) = vi(:);
                end
            end
            v = reshape(v,[size(alpha,1),f.sz]);
            
        end
  
        
        
        function y = eval(f,x,varargin)
            % function y = eval(f,x)
            % Computes evaluations of f at points x
            % f: FunctionalBasisArray
            % x: array of size N-by-d or cell of length d
            H = eval(f.basis,x,varargin{:});
            y = evalWithBasesEvals(f,H,varargin{:});
        end
        
        function y = functionEvalWithBasesEvals(f,H)
            warning('functionEvalWithBasesEvals method will be removed in a future release. Use evalWithBasesEvals instead.')
            y = evalWithBasesEvals(f,H);
        end
        
        function y = evalWithBasesEvals(f,H)
            % function y = evalWithBasesEvals(f,H)
            
            y = H*reshape(f.data,cardinal(f.basis),prod(f.sz));
            y = reshape(y,[size(H,1),f.sz]);
        end
        
        function y = evalDerivative(f,n,x)
            % function y = evalDerivative(f,n,x)
            % Computes the n-derivative of f at points x in R^d, with n a
            % multi-index of size d
            % f: FunctionalBasisArray
            % n: 1-by-d array of integers
            % x: N-by-d array of doubles
            % y: N-by-1 array of doubles
            
            if ~ismethod(f.basis,'evalDerivative')
                error('Not implemented for the basis object.')
            end
            
            H = evalDerivative(f.basis,n,x);
            y = evalWithBasesEvals(f,H);
            
        end
        
        function df = derivative(f,n)
            % function df = derivative(f,n)
            % Computes the n-derivative of f
            % f: FunctionalBasisArray
            % n: 1-by-d array of integers
            % df: FunctionalBasisArray
            
            df = f;
            df.basis = derivative(f.basis,n);
        end
        
        function [y,x] = random(f,varargin)
            % function [y,x] = random(f,n,rv)
            % Computes evaluations of f at an array of points of size n,
            % drawn randomly according to the RandomVector rv if provided,
            % or to the standard RandomVector associated with each
            % polynomial if not
            % f: FunctionalBasisArray
%             % n: i-by-j double
            % rv: RandomVector or RandomVariable (optional)
%             % y: i-by-j double
%             % x: d-by-1 cell containing i-by-j doubles, where d is the
%             % dimension of the basis
            
%             [fx,x] = random(f.basis,varargin{:});
%             y = cell2mat(cellfun(@(x) reshape(x,1,numel(fx{1})),fx,'UniformOutput',false)).';
%             y = y*reshape(f.data,cardinal(f.basis),prod(f.sz));
%             y = reshape(y,size(fx{1}));
            
            [y,x] = random(f.basis,varargin{:});
            y = y*reshape(f.data,cardinal(f.basis),prod(f.sz));
            
            
        end
        
        function rv = getRandomVector(f)
            % function rv = getRandomVector(f)
            % Gets the random vector rv associated with the basis functions of f
            % f: FunctionalBasisArray
            % rv: RandomVector
            
            rv = getRandomVector(f.basis);
        end
        
        function g = projection(f,basis,indices)
            % function g = projection(f,basis,indices)
            % Projection of f on a functional basis using multi-indices
            % indices if provided, or the multi-indices associated with
            % the functional basis if not
            % f: FunctionalBasisArray
            % basis: FunctionalBasis (FullTensorProductFunctionalBasis or SparseTensorProductFunctionalBasis)
            % indices: MultiIndices (optional if basis is an instance of
            % class SparseTensorProductFunctionalBasis, indices = basis.indices by default)
            % g: FunctionalBasisArray
            
            if nargin<3 || isempty(indices)
                if isa(basis,'SparseTensorProductFunctionalBasis')
                    indices = basis.indices;
                else
                    error('Must specify a MultiIndices')
                end
            end
            
            if ndims(f.basis)==ndims(basis) && cardinal(f.basis)<=cardinal(basis)
                d = sparse(cardinal(basis),prod(f.sz));
                [~,ia,ib] = intersectIndices(f.basis.indices,indices);
                d(ib,:) = f.data(ia,:);
                if ndims(f.data)~=ndims(d)
                    d = reshape(full(d),[cardinal(basis),f.sz]);
                end
                if isa(basis,'FullTensorProductFunctionalBasis')
                    H = FullTensorProductFunctionalBasis(basis.bases);
                elseif isa(basis,'SparseTensorProductFunctionalBasis')
                    H = SparseTensorProductFunctionalBasis(basis.bases,indices);
                else
                    error('Not implemented')
                end
                g = FunctionalBasisArray(d,H,f.sz);
            else
                error('Not implemented')
            end
        end
        
        function plotMultiIndices(f,varargin)
            % function plotMultiIndices(f)
            % Plots the multi-index set associated to f
            % f: FunctionalBasisArray
            
            plotMultiIndices(f.basis,varargin{:});
        end
        
        function s = subFunctionalBasis(p)
            % function s = subFunctionalBasis(p)
            % Converts a FunctionalBasisArray p into a SubFunctionalbasis s
            % p: FunctionalBasisArray
            % s: SubFunctionalbasis
            
            s = SubFunctionalBasis(p.basis,p.data);
        end
        
        function s = getCoefficients(f) 
            s = reshape(f.data,[cardinal(f.basis),f.sz]);
        end
        
        %         function p = subsref(p,s)
        %             % function p = subsref(p,s)
        %             % Remplaces the built-in subsref, see doc subsref
        %
        %             % Warning: may cause problems if FunctionalBasisArray inherits
        %             % from Function
        %
        %             if length({s.type}) == 1 && strcmpi(s.type,'()')
        %                 p.sz = length(s.subs{1});
        %                 s.subs = [{':'} s.subs];
        %                 p.data = subsref(p.data,s);
        %             else
        %                 p = builtin('subsref',p,s);
        %             end
        %         end
        %
        %         function p = subsasgn(p,s,b)
        %             % function p = subsasgn(p,s)
        %             % Remplaces the built-in subsasgn, see doc subsasgn
        %
        %             if isa(b,'FunctionalBasisArray')
        %                 b = b.data;
        %             end
        %             if length({s.type}) == 1 && strcmpi(s.type,'()')
        %                 s.subs = [{':'} s.subs{1}];
        %                 p.data = subsasgn(p.data,s,b);
        %             else
        %                  p = builtin('subsasgn',p,s,b);
        %             end
        %         end
    end
    
    methods (Static)
        
    end
end
