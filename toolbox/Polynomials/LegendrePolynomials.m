classdef LegendrePolynomials < OrthonormalPolynomials
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    methods
        function p = LegendrePolynomials(n)
            % function p = LegendrePolynomials(varargin)
            % Polynomials defined on [-1,1], orthonormal with respect to
            % the standard uniform measure 1/2
            % n: integer, indicates the highest degree for which a
            % polynomial can be computed with the stored recurrence
            % coefficients (optional, default value: 50)
            % p: LegendrePolynomials
            
            p.measure= UniformRandomVariable(-1,1);
            
            if ~exist('n','var')
                % If a degree greater than 50 is used, the current method
                % leads to numerical errors
                n = 50;
            end
            
            [p.recurrenceCoefficients, p.orthogonalPolynomialsNorms] = p.recurrence(n);
        end
    end
    
    methods (Static, Hidden)
        
        function [recurr, norms] = recurrence(n)
            % function [recurr, norms] = recurrence(n)
            % Computes the coefficients of the three-term recurrence used
            % to construct the Legendre polynomials, and the norms of the
            % polynomials
            % The three-term recurrence writes:
            % p_{n+1}(x) = (x-a_n)p_n(x) - b_n p_{n-1}(x), a_n and b_n are
            % the three-term recurrence coefficients
            % n: integer
            % recurr: 2-by-n double
            % norms: 1-by-n double
            
            a = zeros(1,n+1);
            b = (0:n).^2 ./ (4*(0:n).^2 - 1);
            
            recurr = sparse([a;b]);
            norms = sqrt(1./(2*(0:n)+1)).*2.^(0:n).*factorial(0:n).^2 ./ factorial(2*(0:n));
        end
    end
end

