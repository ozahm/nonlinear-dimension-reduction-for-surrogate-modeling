classdef IntegrationRule
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        points
        weights
    end
    
    methods
        
        function I = IntegrationRule(points,weights)
            % function I = IntegrationRule(points,weights)
            % points : integration points, array of size nxd
            % weights : integration weights, array of size 1xn
            %
            
            I.points = points;
            I.weights = weights(:).';
        end
        
        function n = ndims(I)
            n = size(I.points,2);
        end
        
        function y = integrate(I,f)
            % function y = integrate(I,f)
            % Integration of function f.
            % call f(x) with an array x of size numel(I)xd, where d is the
            % dimension
            fx = f(I.points);
            y = dot(fx(:),I.weights);
        end
        
        function I = tensorize(I,d)
            p = FullTensorGrid(I.points(:),d);
            w = repmat({I.weights},d,1);
            I = FullTensorProductIntegrationRule(p,w);
        end
    end
    
    
    methods (Static)
        
        function I = gauss(rv,varargin)
            % function gauss(rv,varargin)
            % rv : RandomVariable or RandomVector
            % call the method gaussIntegrationRule of RandomVariable or RandomVector
            
            I = rv.gaussIntegrationRule(varargin{:});
        end
        
    end
end
