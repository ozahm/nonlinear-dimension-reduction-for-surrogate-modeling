classdef FullTensorProductIntegrationRule < IntegrationRule
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    methods
        function G = FullTensorProductIntegrationRule(points,weights)
            % FullTensorProductIntegrationRule : Class constructor
            % function G = FullTensorProductIntegrationRule(points,weights)
            % points : cell of length dim or FullTensorGrid containing 1D points
            % weights : cell of length dim containing 1D weights
            
            G@IntegrationRule(points,weights)
            if isa(points,'cell')
                points = FullTensorGrid(points);
            elseif ~isa(points,'FullTensorGrid')
                error('points must be a FullTensorGrid or a cell')
            end
            if ~isa(weights,'cell') || length(weights)~=ndims(points)
                error('weights must be a cell of length dim')
            end
            G.points = points;
            for k=1:length(weights)
                weights{k} = weights{k}(:);
            end
            G.weights = weights(:);
        end
        
        function n = ndims(I)
            n = ndims(I.points);
        end
        
        function y = integrate(I,f)
            % function y = integrate(I,f)
            % Integration of function f.
            % call f(x) with an array x of size numel(I)xd, where d is the
            % dimension
            x = array(I.points);
            w = weightsOnGrid(I);
            y = w'*f.eval(x);
            
        end
        
        function w = weightsOnGrid(I)
            w = CanonicalTensor(I.weights,1);
            w = double(full(w));
            w = w(:);
            
        end
    end
    
end

