classdef SparseTensorProductIntegrationRule < IntegrationRule
    %
    
    % This file is part of the Matlab Toolbox ApproximationToolbox,
    % developed under the BSD Licence.
    % See the LICENSE file for conditions.
    
    properties
        dim
    end
    
    methods
        function G = SparseTensorProductIntegrationRule(points,weights)
            % class SparseTensorProductIntegrationRule
            % function G = SparseTensorProductIntegrationRule(points,weights)
            % points : SparseTensorGrid
            % weights
            
            G.points = points;
            G.weights = weights;
        end
        
        
        function y = integrate(I,f)
            % function y = integrate(I,f)
            % Integration of function f.
            % call f(x) with an array x of size numel(I)xd, where d is the
            % dimension
            x = array(I.points);
            w = weights(:);
            y = w'*f.eval(x);
            
        end
        
        
        
    end
    
end
