%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Choose the function
% u = BoreholeFunction();
u = cosOfNorm(20);
% u = Oscillator();
% u = Example4();
% u = DeepComposed(3);

RV = RandomVector(u.measure.measures);
dim = u.dim;

% %% or... the bridge!
% 
% dim = 40;
% indEigs = 1;
% hmax = 3;
% u = MorandiBridge(dim,indEigs,hmax);
% RV = RandomVector(u.measure.measures);
% u.plotMesh()
% disp('done.')

% %% Pretreatment
% 
% N = 1000;
% X = RV.random(N);
% guX = u.evalGradient(X);
% 
% [U,S,V]=svd(guX);
% S = diag(S);
% ind2keep = S>max(S)*1e-7;
% ind2freeze = S<=max(S)*1e-7;
% 
% X0 = RV.random(1);

%% Draw the sample {Xi,u(Xi),grad_u(Xi)}

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
Jg = CostFunction_FeatureMap(X,guX);

N_validation = 1000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);
Jg_validation = CostFunction_FeatureMap(X_validation,guX_validation);

%% Construct g

m = 3;
opts = CostFunction_FeatureMap.OMP_default_opts();
opts.maxit = 100;
opts.quasi_Newton_opts.compute_Sigma_every = 3;
opts.quasi_Newton_opts.increment_tol = 1e-100;
opts.quasi_Newton_opts.dJ_tol = 1e-100;
opts.quasi_Newton_opts.maxit = 50;

g = SmoothFeatureMap.randn(RV,m);
[g,Jg_iter,output_g] = Jg.OMP_CV(g,opts);
% [g,Jg_iter,output_g] = Jg.OMP(g,opts);
% g = Jg.active_subspace_map(g)

for i=1:5
    semilogy(output_g.J_test{i})
    hold on
end
hold off

%% Monitor OMP_g

Jg_duringOMP = zeros(length(output_g.g),1);
for i=1:length(output_g.g)
    Jg_duringOMP(i) = Jg_validation.eval(output_g.g{i});
end
semilogy(Jg_duringOMP)



%% Construct f

Jf = CostFunction_Profile(g,X,uX,guX);

opts = CostFunction_Profile.OMP_default_opts();
opts.maxit = 100;
f = SmoothProfile.randn(m);
[f,J_iter,output_f] = Jf.OMP_CV(f,opts);

output_f.maxit_CV

%% Monitor OMP_f

Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
error_SameTraning = zeros(length(output_f.f),1);

for i=1:length(output_f.f)
    error_SameTraning(i) = Jf_validation.eval(output_f.f{i});
end

semilogy(error_SameTraning)

%%  Without dimension reduction

gid = SmoothFeatureMap.identity(RV);
Jv_validation = CostFunction_Profile(gid,X_validation,uX_validation,guX_validation,1);

Jv = CostFunction_Profile(gid,X,uX,guX,1);
opts = CostFunction_Profile.OMP_default_opts();
opts.maxit = 200;
[v,Jv_iter,output_v] = Jv.OMP_CV(SmoothProfile.randn(dim),opts,1);

output_v.maxit_CV


%%
Jv_true = zeros(numel(output_v.f),1);
Jv_true_L2 = zeros(numel(output_v.f),1);
for j=1:numel(output_v.f)
    Jv_validation.with_derivative = 0;
    Jv_true(j) = Jv_validation.eval(output_v.f{j});
end 
semilogy(Jv_true)






