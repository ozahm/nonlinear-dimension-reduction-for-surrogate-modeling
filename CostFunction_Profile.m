classdef CostFunction_Profile
    
    % Cost function for the profile
    % 
    %   J(f) = E[|| u(X) - fog(X) ||^2] ...
    %        + E[|| \nabla u(X) - \nabla fog(X) ||^2]
    % 
    
    
    properties
        
        N % Sample size
        X % N-by-d array of doubles
        uX % N-by-1 array of doubles
        grad_uX % N-by-d array of doubles
        
        with_derivative % logical
        
        g % SmoothFeatureMap %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        gX % N-by-m double
        grad_gX % N-by-1 cell containing dim-by-m double
        
    end
    
    methods
        %------------------------------------------------------------------
        function self = CostFunction_Profile(g,X,uX,grad_uX,with_derivative)
            
            if nargin<5
                with_derivative = true;
            end
            self.g = g;
            [self.gX,self.grad_gX] = g.eval(X);
            self.X = X;
            self.uX = uX;
            self.grad_uX = grad_uX;
            self.N = size(X,1);
            self.with_derivative = with_derivative;
            
        end
        %------------------------------------------------------------------
        function [J,grad_J] = eval(self, f, varargin)
            %  function [J,grad_J] = eval(self, f, ind_subsample)
            % Evaluate the least-square function and its gradient. If 
            % "ind_subsample" is specified, the sample is sub-sampled.
            
            [A,y,N_] = get_least_squares_system(self, f, varargin{:});
            J = norm(A*f.coef-y)^2/N_;
            grad_J = 2*(A'*(A*f.coef-y))/N_;
            
        end
        %------------------------------------------------------------------
        function [A,y,N_] = get_least_squares_system(self, f, FZ, grad_FZ, ind_subsample)
            %  function [A,y,N_] = get_least_squares_system(self, f, ind_subsample)
            % Forms the least squares matrix A and the right-hand side y
            % associated with the cost funciton. If "ind_subsample" is
            % specified, the sample is sub-sampled. The last output
            % argument is the length of (ind_subsample)
            
            N_ = self.N;
            uX_ = self.uX;
            grad_uX_ = self.grad_uX;
            gX_ = self.gX;
            grad_gX_ = self.grad_gX;
            
            if nargin == 5
                N_ = length(ind_subsample);
                uX_ = uX_(ind_subsample);
                grad_uX_ = grad_uX_(ind_subsample);
                grad_gX_ = grad_gX_(ind_subsample);
                FZ = FZ(ind_subsample,:);
                grad_FZ = grad_FZ(ind_subsample);
            end
            
            if self.with_derivative
                if nargin<4
                    [~,~,FZ,grad_FZ] = f.eval(gX_);
                end
                % Gradient-enhanced least squares
                A = mat2cell(FZ,ones(N_,1),size(FZ,2));
                A = cellfun(@(x,y,z)[x;y*z'],A,grad_gX_,grad_FZ,'UniformOutput',0);
                A = vertcat(A{:});
                
                y = mat2cell([uX_ grad_uX_],ones(N_,1),self.g.dim+1);
                y = cellfun(@(y)y',y,'UniformOutput',0);
                y = vertcat(y{:});
                
            else
                % Standard least squares
                if nargin<3
                    [~,~,A] = f.eval(gX_);
                end
                y = uX_;
            end
            
        end
        %------------------------------------------------------------------
        %----------------- OPTIMIZATION PROCEDURES ------------------------
        %------------------------------------------------------------------
        function [f,J] = minimize(self,f,varargin)
            %  function [f,J] = minimize(self,f,ind_subsample)
            % Solves the least squares problem associated with the cost 
            % funciton. If "ind_subsample" isspecified, the sample is 
            % sub-sampled.
            
            [A,y,N_] = get_least_squares_system(self, f, varargin{:});
            
            [Q,R] = qr(A,0);
            f.coef = R\(Q'*y);
            J = norm(A*f.coef-y)^2/N_;
            
        end
        %------------------------------------------------------------------
        function [f,J_iter,output] = OMP(self,f,opts)
            
            if nargin<3
                opts = CostFunction_Profile.OMP_default_opts();
            end
            
            J_iter = zeros(opts.maxit,1);
            f_iter = cell(opts.maxit,1);
            
            % Get A and A_margin
            f_tmp = f.get_marginExtended_Profile();
            [A,y,N_] = self.get_least_squares_system(f_tmp);
            A_margin = A(:,(f.F.cardinal()+1):end);
            A = A(:,1:f.F.cardinal());
            if self.with_derivative
                gram_margin = full(diag(f_tmp.H1_gram_matrix()));
                gram_margin = gram_margin((f.F.cardinal()+1):end);
            else
                gram_margin = ones(size(A_margin,2),1);
            end
            
            
            for i=1:opts.maxit
                
                % Minimize the LS cost function
                [Q,R] = qr(A,0);
                coef = R\(Q'*y);
                f.coef = coef;
                f_iter{i} = f;
                
                % Compute the gradient on the reduced margin
                r = A*coef-y;
                J_iter(i) = norm(r)^2/N_;
                grad_J = (2/N_)*(A_margin'*r);
                grad_J = grad_J./sqrt(gram_margin);% take into account the orthogonalization
                [grad_J,ind] = sort(grad_J.^2,'descend');
                nbOfNewParameter = 1+sum( cumsum(grad_J)/sum(grad_J) < opts.OMP_pctageOfgradRecovery );
                ind = ind(1:min(length(ind),min(nbOfNewParameter,opts.numberOfNewTermsPerIteration)));
                
                % Update f
                [f,new_in_margin] = f.get_marginExtended_Profile(ind);
                if any(any( new_in_margin.array' >= cellfun(@(H)max(H.indices),f.F.bases.bases) ))
                    output.exit_flag = 'Explosion of the polynomial order';
                    output.f = f_iter(1:i);
                    J_iter = J_iter(1:i);
                    return
                end
                % Add ind in A and remove it from A_margin
                A = [A , A_margin(:,ind)];
                A_margin(:,ind) = [];
                gram_margin(ind) = [];
                
                % Add the new margin terms in the current margin
                f_tmp = f;
                f_tmp.F.indices = new_in_margin;
                for j=1:f_tmp.m
                    f_tmp.grad_F{j}.indices = new_in_margin;
                end
                f_tmp.coef = zeros(new_in_margin.cardinal(),1);
                A_newInMargin = self.get_least_squares_system(f_tmp);
                if self.with_derivative
                    gram_newInMargin = full(diag(f_tmp.H1_gram_matrix()));
                else
                    gram_newInMargin = ones(size(A_newInMargin,2),1);
                end
                A_margin = [A_margin, A_newInMargin];
                gram_margin = [gram_margin; gram_newInMargin];
            end
            
            if i==opts.maxit
                output.exit_flag = 'Maximum number of iteration attained.';
            end
            i = i+1;
            
            % Solve one last time on the lower-set
            [Q,R] = qr(A,0);
%             R = R(:,ind2keep);
%             Q = Q(:,ind2keep);
            coef = R\(Q'*y);
            J_iter(i) = norm(A*coef-y)^2/N_;
            f.coef = coef;
            f_iter{i} = f;
            
            % Store 
            output.f = f_iter(1:i);
            J_iter = J_iter(1:i);
        end
        %------------------------------------------------------------------
        function [f,J_iter,output] = OMP_CV(self,f,opts,display)
            
            if nargin<3 || isempty(opts)
                opts = CostFunction_Profile.OMP_default_opts();
            end
            if nargin<4
                display = 1;
            end
            
            % Determine maxit by making sure we do not overfit
            N_train = floor(self.N*(opts.nb_CV_fold-1)/opts.nb_CV_fold);
            if self.with_derivative
                maxit_max = floor(0.75*N_train*(f.m + 1) / opts.numberOfNewTermsPerIteration);
            else
                maxit_max = floor(0.75*N_train / opts.numberOfNewTermsPerIteration);
            end
            opts.maxit = min( max(1,maxit_max) , opts.maxit);
            
            % Run the nu-fold method to determine "maxit"
            J_train = cell(opts.nb_CV_fold,1);
            J_test = cell(opts.nb_CV_fold,1);
            perm = randperm(self.N);
            for i=1:opts.nb_CV_fold
                if display
                    disp(['Cross-validation for f: ' num2str(i) '/' num2str(opts.nb_CV_fold)])
                end
                % train/test splitting
                fold_size = floor(self.N/opts.nb_CV_fold);
                test = (i-1)*fold_size+1:i*fold_size;
                train = setdiff(1:self.N, test);
                test = perm(test);
                train = perm(train);
                
                % Training
                X_train = self.X(train,:);
                uX_train = self.uX(train);
                guX_train = self.grad_uX(train,:);
                J_train{i} = CostFunction_Profile(self.g,X_train,uX_train,guX_train,self.with_derivative);
                [~,J_train{i},output_train] = J_train{i}.OMP(f,opts);
                
                % Test
                X_test = self.X(test,:);
                uX_test = self.uX(test);
                guX_test = self.grad_uX(test,:);
                J_test{i} = CostFunction_Profile(self.g,X_test,uX_test,guX_test,0);
                J_test{i} = cellfun(@(f) J_test{i}.eval(f), output_train.f );
                
            end
            
            % Determine the maxit which minimizes J_test
            maxit = zeros(max(cellfun(@(x)numel(x),J_test)),1);
            for i=1:length(maxit)
                tmp = cellfun(@(x)x(min(i,numel(x))),J_test,'UniformOutput',1);
                maxit(i) = mean(log(tmp));
            end
            [~,maxit] = min(maxit);
            
            % Run the complete algorithm with "maxit"
            opts.maxit = maxit;
            [f,J_iter,output] = self.OMP(f,opts);
            output.maxit_CV = maxit;
            output.J_test = J_test;
        end
        %------------------------------------------------------------------
    end
    methods (Static)
        %------------------------------------------------------------------
        function opts = OMP_default_opts()
            opts = struct( ...
                'maxit', 1000, ...
                'numberOfNewTermsPerIteration',20,...
                'OMP_pctageOfgradRecovery',0.3,...
                'nb_CV_fold',5);
        end 
        %------------------------------------------------------------------
    end
end
