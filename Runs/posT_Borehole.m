%% Initialize the problem
clc
clear classes
clear all
addpath(genpath('.'));
% load(['Runs/data/run_Borehole_27-Nov-2019.mat']);
% load(['Runs/data/run_Borehole_26-Sep-2020.mat']);


%%
ind =0;
%% Plot one realization at the time
ind = ind+1;
toplot = exp(squeeze(mean(log(error_L2_validation(ind,:,:)),1)))';
semilogy(MM,toplot)
title('Nonlinear feature maps')
% axis([1 8 1e-10 1e3])
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0))
xlabel('m')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')


%% Plot...
ind = 8;
subplot(1,3,1)
toplot = exp(squeeze(mean(log(error_L2_validation(ind,:,:)),1)))';
semilogy(MM,toplot)
title('Nonlinear feature maps')
% axis([1 8 1e-10 1e3])
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0))
xlabel('m')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

subplot(1,3,2)
toplot = exp(squeeze(mean(log(error_L2_validation_AS(ind,:,:)),1)))';
semilogy(MM,toplot)
title('Linear feature maps')
% axis([1 8 1e-10 1e3])
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0))
xlabel('m')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

subplot(1,3,3)
toplot = exp(squeeze(mean(log(error_L2_validation_DGSM(ind,:,:)),1)))';
semilogy(MM,toplot)
title('DGSM maps')

% axis([1 8 1e-10 1e3])
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0))
xlabel('m')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

%% Plot each realization 
n=4;
disp(['N=' num2str(NN(n))])
col = distinguishable_colors(3);
ind = [1:10];


for i=ind
    
    toplot = exp(squeeze(mean(log(error_L2_validation(i,n,:)),1)))';
    semilogy(MM,toplot,'col',col(1,:))
    hold on
    
    toplot = exp(squeeze(mean(log(error_L2_validation_AS(i,n,:)),1)))';
    semilogy(MM,toplot,'col',col(2,:))
    
    toplot = exp(squeeze(mean(log(error_L2_validation_DGSM(i,n,:)),1)))';
    semilogy(MM,toplot,'col',col(3,:))
    
end
legend({'Nonlinear DR','AS','DGSM'})
title(['N=' num2str(NN(n))])
hold off

%% or
clf
toplot = exp(squeeze(mean(log(error_L2_validation),1)))';
semilogy(toplot,'-')
hold on
toplot = exp(squeeze(mean(log(error_L2_validation_AS),1)))';
semilogy(toplot,'--')
hold off

%%

col = distinguishable_colors(length(NN));

toplot = error_L2_validation;
for j=1:length(NN)
    tmp = squeeze(toplot(:,j,:));
    semilogy_std(MM,tmp,'color',col(j,:),'LineStyle','-')
    hold on
end
hold off

title('Nonlinear feature maps')
axis([1 8 1e-7 1e2])
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0),'Location','SouthWest')
xlabel('m')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

%% SAVE
my_matlab2tikz('Plot/Borehole/','Nonlinear.tex')
