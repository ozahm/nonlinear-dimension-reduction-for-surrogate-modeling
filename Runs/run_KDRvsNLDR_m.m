%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Initialize the model

u = BoreholeFunction();
% u = Oscillator();
% u = Example4();
% u = DeepComposed(3);
% u = cosOfNorm(5);

RV = RandomVector(u.measure.measures);
dim = u.dim;

%% Draw the Traning and Validation sets

K = 20 ; % Number of experiments
NN = [30 60 150 300];
N_validation = 2000;
MM = [1:8];

XX = cell(K,length(NN));
for i=1:K
    for j=1:length(NN)
        N = NN(j);
        XX{i,j} = RV.random(N);
    end
end

X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);

%%

error_L2_validation_KDR = zeros(K,length(NN),length(MM),3);
error_L2_validation = zeros(K,length(NN),length(MM),3);

opts_Jf = CostFunction_Profile.OMP_default_opts();
opts_Jf.numberOfNewTermsPerIteration = 10;
opts_Jf.OMP_pctageOfgradRecovery=1;
opts_Jf.maxit = 500;

for i=1:K % K, experiment
    disp(['Experiment n' num2str(i) ' out of ' num2str(K)])
    
    for j=1:length(NN) % N, sample size
        disp(['  Sample size ' num2str(NN(j))])
        
        X = XX{i,j};
        uX  = u.eval(X);
        guX = u.evalGradient(X);
        Jg  = CostFunction_FeatureMap(X,guX);
        
%         Y = uX;
%         Y = [guX];
        Y = [uX guX];
        
        for k=1:length(MM) % M, intermediate dimension
            disp(['    Intermediate dimension ' num2str(MM(k))])
            
            m = MM(k);
            [g_cubic,~,~,g_quadratic,g_linear] = Jg.cubicMap(RV,m);
            mMax = [size(g_linear.coef,1),size(g_quadratic.coef,1),size(g_cubic.coef,1)];
            
            for degree = 1:3 % total degree for g
                
                %%% Construct g via min J(g)
                if degree==1
                    g = g_linear;
                elseif degree==2
                    g = g_quadratic;
                elseif degree==3
                    g = g_cubic;
                end
                Jf = CostFunction_Profile(g,X,uX,guX);
                f = Jf.OMP_CV(SmoothProfile.randn(m),opts_Jf,0);
                % Validation error
                Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
                error_L2_validation(i,j,k,degree) = Jf_validation.eval(f);
                
                %%% Construct g via KDR
                gKDR = KDR(Y,X,g);
                Jf = CostFunction_Profile(gKDR,X,uX,guX);
                Jf.with_derivative = 1;
                fKDR = Jf.OMP_CV(SmoothProfile.randn(m),opts_Jf,0);
                % Validation error
                Jf_validation = CostFunction_Profile(gKDR,X_validation,uX_validation,guX_validation,0);
                error_L2_validation_KDR(i,j,k,degree) = Jf_validation.eval(fKDR);
                
                
            end
        end
    end
end


%%
% save(['Runs/data/run_KDR_Borehole_' date '.mat'],'-v7.3')
load('Runs/data/run_KDR_Borehole_22-Sep-2021.mat')

%% PostT


for degree=1:3
    
    subplot(1,3,degree)
    % error_L2_validation
    % error_L2_validation_PCA
    
    for j=1:length(NN) % N, sample size
        
        
        yplot = error_L2_validation(1,j,:,degree);
        yplot = squeeze(yplot);
        semilogy(MM,yplot);
        hold on
        
        yplot = error_L2_validation_KDR(1,j,:,degree);
        yplot = squeeze(yplot);
        semilogy(MM,yplot);
%             semilogy_std_shade
        hold on
    end
    hold off
    
    axis([1 max(MM) 1e-7 1e4])
    
end


