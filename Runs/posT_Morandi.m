%% Initialize the problem
clc
clear classes
clear all
addpath(genpath('.'));
load(['Runs/data/run_morandi_18-Nov-2019.mat']);

%% Plot...

subplot(1,2,1)
toplot = exp(squeeze(mean(log(error_L2_validation),1)))';
semilogy(MM,toplot)
title('Nonlinear')

subplot(1,2,2)
toplot = exp(squeeze(mean(log(error_L2_validation_AS),1)))';
semilogy(MM,toplot)
title('Linear')


%% or
clf
toplot = exp(squeeze(mean(log(error_L2_validation),1)))';
semilogy(MM,toplot,'-')
hold on
toplot = exp(squeeze(mean(log(error_L2_validation_AS),1)))';
semilogy(MM,toplot,'--')
hold off

%%
col = distinguishable_colors(2);

toplot = error_L2_validation_AS;
for j=1:length(NN)
    tmp = squeeze(toplot(:,j,:));
    semilogy_std(MM,tmp,'color',col(1,:),'LineStyle','--')
    hold on
end
hold off

hold on
toplot = error_L2_validation;
for j=1:length(NN)
    tmp = squeeze(toplot(:,j,:));
    semilogy_std(MM,tmp,'color',col(2,:),'LineStyle','-')
    hold on
end
hold off


hold on
semilogy_std(MM,repmat(Jv',length(MM),1),'color',[0,0,0],'LineStyle','-')

%% Compare with flat approx (no dimension reduction, just the polynomial approximation)


gid = SmoothFeatureMap.identity(RV);

Jv = zeros(K,1);
for i=1:K

uX = u.eval(X{i});
guX = u.evalGradient(X{i});
Jv_ = CostFunction_Profile(gid,X{i},uX,guX,1);
opts = CostFunction_Profile.OMP_default_opts();
opts.maxit = 500;
[v,Jv_iter,output_v] = Jv_.OMP_CV(SmoothProfile.randn(dim),opts,0);

Jv_ = CostFunction_Profile(gid,X_validation,uX_validation,guX_validation,1);
Jv_.with_derivative = 0;
Jv(i) = Jv_.eval(v);

end

%%

toplot = error_L2_validation;

mean_ = zeros(length(MM),1);
std_ = zeros(length(MM),1);
for i=1:length(MM)
    tmp = (squeeze(toplot(:,1,i)));
    mean_(MM(i)) = exp(mean(log(tmp)));
    std_(MM(i)) = exp(std(log(tmp)));
%     mean_(MM(i)) = mean(tmp);
%     std_(MM(i)) = std(tmp);
end

%%

MM_ = [2:2:10];

aa = arrayfun(@(x) ['$m=' num2str(x) '$ & '], MM_,'UniformOutput',0 );
bb = arrayfun(@(x,y) ['$' my_num2str(x) '(\pm ' my_num2str(x*(y-1)) ')$ & '], mean_(MM_),std_(MM_),'UniformOutput',0 );


nia = 6.7207e-13;
nia2 = 1.033549e-12;
nia2 = exp((nia2/nia)^2 /3);

aa = [aa{:} '$m=d$ \\' ]
bb = [bb{:} '$' my_num2str(nia) '(\pm ' my_num2str(nia*(nia2-1)) ')$ & ']
%%
aa = arrayfun(@(m,x,y) ['$m=' num2str(m) '$ & $' my_num2str(x) '(\pm ' my_num2str(x*(y-1)) ')$ \\ '], MM_',mean_(MM_),std_(MM_),'UniformOutput',0 );
% bb = arrayfun(@(m,x,y) ['$' my_num2str(x) '(\pm ' my_num2str(x*(y-1)) ')$ & '], mean_(MM_),std_(MM_),'UniformOutput',0 );

% my_matlab2tikz('Plot/Morandi/','data.tex')



