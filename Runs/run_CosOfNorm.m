%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Choose the function
dim = 20;
u = cosOfNorm(dim);
RV = RandomVector(u.measure.measures);

%% Draw the sample {Xi,u(Xi),grad_u(Xi)}
K = 5; % number of trials
N = 100; % Sample size
MM = [1 5 10]; % values of m

Jg_iter = cell(numel(MM),K);
output_g = cell(numel(MM),K);
J_iter = cell(numel(MM),K);
output_f = cell(numel(MM),K);
f = cell(numel(MM),K);
g = cell(numel(MM),K);
X = cell(K,1);
for k=1:K
    tic
    disp(['Number of trial: ' num2str(k) ' over ' num2str(K)]);
    
    X{k} = RV.random(N);
    uX = u.eval(X{k});
    guX = u.evalGradient(X{k});
    Jg = CostFunction_FeatureMap(X{k},guX);

    for i = 1:numel(MM)
        
        fprintf('.')
        m = MM(i);
        
        opts = CostFunction_FeatureMap.OMP_default_opts();
        opts.numberOfNewTermsPerIteration = 1;
        g{i,k} = SmoothFeatureMap.randn(RV,m);
        [g{i,k},Jg_iter{i,k},output_g{i,k}] = Jg.OMP_CV(g{i,k},opts,0);
        
        % Construct f
        Jf = CostFunction_Profile(g{i,k},X{k},uX,guX);
        opts = CostFunction_Profile.OMP_default_opts();
%         opts.numberOfNewTermsPerIteration = 1;
        f{i,k} = SmoothProfile.randn(m);
        [f{i,k},J_iter{i,k},output_f{i,k}] = Jf.OMP_CV(f{i,k},opts,0);
        
    end
    t = toc;
    disp(['------------------> ETA: ' datestr(datetime()+seconds(t*(K-k)),'HH:MM:SS')])
end

%% Validation

N_validation = 1000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);

Jg_validation = CostFunction_FeatureMap(X_validation,guX_validation);
Jg_true = cell(numel(MM),K);
Jfg_true_L2 = cell(numel(MM),K);

for k=1:K
    tic
    disp(['Number of trial: ' num2str(k) ' over ' num2str(K)]);
    
    for i=1:numel(MM)
        
        Jg_true{i,k} = zeros(numel(output_g{i,k}),1);
        for j=1:numel(output_g{i,k}.g)
            Jg_true{i,k}(j) = Jg_validation.eval(output_g{i,k}.g{j});
        end
        
        Jf_validation = CostFunction_Profile(g{i,k},X_validation,uX_validation,guX_validation);
        Jfg_true_L2{i,k} = zeros(numel(output_f{i,k}.f),1);
        Jf_validation.with_derivative = 0;
        for j=1:numel(output_f{i,k}.f)
            Jfg_true_L2{i,k}(j) = Jf_validation.eval(output_f{i,k}.f{j});
        end
    end
    t = toc;
    disp(['------------------> ETA: ' datestr(datetime()+seconds(t*(K-k)),'HH:MM:SS')])
end

% %% Plot J(g)
% 
% col = distinguishable_colors(numel(MM));
% for k=1:K
%     for i=1:numel(MM)
%         semilogy(Jg_true{i,k},'color',col(i,:),'LineStyle','-')
%         hold on
%         semilogy(Jg_iter{i,k},'color',col(i,:),'LineStyle','--','HandleVisibility','off')
%     end
% end
% hold off
% legend(arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0), 'Location','SouthWest')
% axis([0 21, 1e-8 1])
% xlabel('OMP iterations for $g$')
% ylabel('$J(g)$')
% % %% SAVE
% % my_matlab2tikz('Plot/CosOfNorm_Jg_OMP/','data.tex')

%% Compute the approximation without dimension reduction

gid = SmoothFeatureMap.identity(RV);
Jv_validation = CostFunction_Profile(gid,X_validation,uX_validation,guX_validation,1);

output_v = cell(K,1);
Jv_true_L2 = cell(K,1);
for i=1:K
    disp(i)
    uX = u.eval(X{i});
    guX = u.evalGradient(X{i});
    Jv = CostFunction_Profile(gid,X{i},uX,guX,1);
    opts = CostFunction_Profile.OMP_default_opts();
    opts.maxit = 1000;
    [v,~,output_v{i}] = Jv.OMP_CV(SmoothProfile.randn(dim),opts,1);
    for j=1:numel(output_v{i}.f)
        
        Jv_validation.with_derivative = 0;
        Jv_true_L2{i}(j) = Jv_validation.eval(output_v{i}.f{j});
    end
    
end

%%
save(['Runs/data/run_CosOfNorm_' date '.mat'],'-v7.3')


% 
% %% Plot J(f)
% 
% col = distinguishable_colors(numel(MM)+1);
% col([6 4],:) = col([4 6],:);
% 
% for k=1:K
%     for i=1:numel(MM)
% %         semilogy(Jfg_true_H1{i,k},'color',col(i,:),'LineStyle','-')
%         semilogy(Jfg_true_L2{i,k},'color',col(i,:),'LineStyle','-')
%         hold on
% %         semilogy(J_iter{i,k},'color',col(i,:),'LineStyle','--','HandleVisibility','off')
%     end
% %     semilogy(Jv_true{i,k},'color',[0 0 0],'LineStyle','-')
%     semilogy(Jv_true_L2{k},'color',col(i+1,:),'LineStyle','-','linewidth',1.2)
% %     semilogy(Jv_iter{k},'color',[0 0 0],'LineStyle','--','HandleVisibility','off')
% end
% 
% hold off
% L = arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0);
% legend([L,{'$m=d$'}], 'Location','SouthEast')
% xlabel('OMP iterations for $f$')
% ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')
% 
% axis([1 250 1e-6 1])
% %%
% %%% SAVE
% my_matlab2tikz('Plot/CosOfNorm_Jf_OMP/','data.tex')
% 
