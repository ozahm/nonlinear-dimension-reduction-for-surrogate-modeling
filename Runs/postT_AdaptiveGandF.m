%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%%

load('Runs/data/run_AdaptiveGandF_Borehole_23-Oct-2020.mat')
ind = [1 2 3];
NN = NN(ind);
error_SameSample_GradFree_validation = error_SameSample_GradFree_validation(:,ind,:);
error_SameSample_validation = error_SameSample_validation(:,ind,:);

%%

load('Runs/data/run_AdaptiveGandF_DeepComposed16_26-Oct-2020.mat')

%%

% Jg_poly_validation 
% error_validation 
% error_GradFree_validation 
% error_SameSample_validation 
% error_SameSample_GradFree_validation 

%%

toplot = error_SameSample_GradFree_validation;
% toplot = error_SameSample_validation ;
% toplot = error_validation ;
% toplot = error_GradFree_validation ;

col = distinguishable_colors(length(NN));

lw = 1.5;
for i=1:length(NN)
    semilogy_std_shade(MM,squeeze(toplot(:,i,:)),'color',col(i,:),'LineWidth',lw)
    hold on
end

toplot = Jg_adapt_validation;
toplot(toplot==0) = 1e-20;
for i=1:length(NN)
    semilogy_std_shade(MM,squeeze(toplot(1:K,i,:)),'color',col(i,:),'LineStyle','-.','LineWidth',lw)
    hold on
end

legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0),'Location','SouthWest')
hold off

axis([1 8 1e-7 1e1])
% axis([1 16 2e-6 2e-4])

xlabel('$m$')
% my_matlab2tikz('Plot/DeepComposed/','DeepComposed_AdaptiveGandF_GradFreeRegression.tex') % _GradFreeRegression
% my_matlab2tikz('Plot/Borehole/','Borehole_AdaptiveGandF.tex') % _GradFreeRegression
%%
clf
toplot = cellfun(@(x)x.nbCoef(1)-1 , output_g);
% yyaxis left
for i=1:length(NN)
    semilogy(MM,squeeze(mean(toplot(:,i,:),1)),'color',col(i,:),'Linestyle','-')
%     semilogy_std_shade(MM,squeeze((toplot(:,i,:))),'color',col(i,:))
%     semilogy_std(MM,squeeze((toplot(:,i,:))),'color',col(i,:))
    hold on
end
ylabel('$\#\Lambda_K$')
xlabel('$m$')
% axis([1,16,16,90])
hold off
legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0),'Location','NorthEast')
% my_matlab2tikz('Plot/DeepComposed/','DeepComposed_AdaptiveGandF_CardinalG.tex') % _GradFreeRegression
my_matlab2tikz('Plot/Borehole/','Borehole_AdaptiveGandF_CardinalG.tex') % _GradFreeRegression
%%
toplot = output_f_OldSample;
toplot = cellfun(@(x)x.nbCoef(1)-1 , toplot );
for i=1:length(NN)
    semilogy(MM,squeeze(mean(toplot(:,i,:),1)),'color',col(i,:))
%     semilogy_std_shade(MM,squeeze((toplot(:,i,:))),'color',col(i,:))
%     semilogy_std(MM,squeeze((toplot(:,i,:))),'color',col(i,:))
    hold on
end
hold off
ylabel('$\#\Gamma_L$')
xlabel('$m$')
% axis([1,16,1,2000])

legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0),'Location','NorthWest')

% my_matlab2tikz('Plot/DeepComposed/','DeepComposed_AdaptiveGandF_CardinalF.tex') % _GradFreeRegression
my_matlab2tikz('Plot/Borehole/','Borehole_AdaptiveGandF_CardinalF.tex') % _GradFreeRegression

%%

indK = 1;
indN = 2;

semilogy(MM,squeeze(error_SameSample_validation(indK,indN,:)))
%%
subplot(2,2,1)
semilogy(MM,squeeze(error_validation(indK,indN,:,:)))
subplot(2,2,2)
semilogy(MM,squeeze(error_GradFree_validation(indK,indN,:,:)))
subplot(2,2,3)
semilogy(MM,squeeze(error_SameSample_validation(indK,indN,:,:)))
subplot(2,2,4)
semilogy(MM,squeeze(error_SameSample_GradFree_validation(indK,indN,:,:)))