%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%%

load('Runs/data/run_FixedG_newAdaptiveF_Borehole_27-Oct-2020.mat')
ind = [1 2 4];
NN = NN(ind);
error_SameSample_GradFree_validation = error_SameSample_GradFree_validation(:,ind,:,:);
error_SameSample_validation = error_SameSample_validation(:,ind,:,:);
error_validation = error_validation(:,ind,:,:);
error_GradFree_validation = error_GradFree_validation(:,ind,:,:);

%%

% Jg_poly_validation 
% error_validation 
% error_GradFree_validation 
% error_SameSample_validation 
% error_SameSample_GradFree_validation 

%%
% toplot = error_SameSample_GradFree_validation;
% toplot = error_SameSample_validation ;
toplot = error_validation ;
% toplot = error_GradFree_validation ;

col = distinguishable_colors(3);

indDeg = 3;
% indN = 4;
lw = 1.5;
for i=1:length(NN)
    semilogy_std_shade(MM,squeeze(toplot(1:K,i,:,indDeg)),'color',col(i,:),'LineWidth',lw)
    hold on
end


toplot = Jg_poly_validation;
toplot(toplot==0) = 1e-20;
for i=1:length(NN)
    semilogy_std_shade(MM,squeeze(toplot(1:K,i,:,indDeg)),'color',col(i,:),'LineStyle','-.','LineWidth',lw)
    hold on
end

legend(arrayfun(@(x)['$N=' num2str(x) '$'],NN,'UniformOutput',0),'Location','SouthWest')
hold off
axis([1 8 1e-7 1e1])

xlabel('$m$')

my_matlab2tikz('Plot/Borehole/',['Borehole_FixedG_degree' num2str(indDeg) '.tex'])
%%

indK = 3;
indN = 3;

subplot(2,2,1)
semilogy(MM,squeeze(error_validation(indK,indN,:,:)))
subplot(2,2,2)
semilogy(MM,squeeze(error_GradFree_validation(indK,indN,:,:)))
subplot(2,2,3)
semilogy(MM,squeeze(error_SameSample_validation(indK,indN,:,:)))
subplot(2,2,4)
semilogy(MM,squeeze(error_SameSample_GradFree_validation(indK,indN,:,:)))