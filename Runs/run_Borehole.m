%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Initialize the model

% u = BoreholeFunction();
% u = Oscillator();
% u = Example4();
u = DeepComposed(3);

RV = RandomVector(u.measure.measures);
dim = u.dim;

%% Validation set...

N_validation = 2000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);

%% Draw the sample {Xi,u(Xi),grad_u(Xi)}
t0 = datetime();

K = 10;
NN = [30 60 100 300];
% NN = [50 200 500];
NbOfNewTerms = [1 1 1 1];
MM = [dim:-1:1];


X = cell(K,numel(NN));

Jf_iter_DGSM = cell(K,numel(NN),numel(MM));
error_H1_DGSM = zeros(K,numel(NN),numel(MM));
error_L2_DGSM = zeros(K,numel(NN),numel(MM));
error_H1_validation_DGSM = zeros(K,numel(NN),numel(MM));
error_L2_validation_DGSM = zeros(K,numel(NN),numel(MM));

Jf_iter_AS = cell(K,numel(NN),numel(MM));
error_L2_AS = zeros(K,numel(NN),numel(MM));
error_H1_AS = zeros(K,numel(NN),numel(MM));
error_L2_validation_AS = zeros(K,numel(NN),numel(MM));
error_H1_validation_AS = zeros(K,numel(NN),numel(MM));

Jg_iter = cell(K,numel(NN),numel(MM));
Jf_iter = cell(K,numel(NN),numel(MM));
outputg = cell(K,numel(NN),numel(MM));
error_L2 = zeros(K,numel(NN),numel(MM));
error_H1 = zeros(K,numel(NN),numel(MM));
error_L2_validation = zeros(K,numel(NN),numel(MM));
error_H1_validation = zeros(K,numel(NN),numel(MM));


for i=1:K
    tic
    disp(['Trial #' num2str(i) ' over ' num2str(K)])
    
    for j=1:length(NN)
        
        N = NN(j);
        fprintf(['    N=' num2str(N)])
        X{i,j}   = RV.random(N);
        uX  = u.eval(X{i,j});
        guX = u.evalGradient(X{i,j});
        
        X_f{i,j} = RV.random(N);
        uX_f = u.eval(X_f{i,j});
        guX_f = u.evalGradient(X_f{i,j});
        
        Jg  = CostFunction_FeatureMap(X{i,j},guX);
        
        

        parfor k=1:length(MM)
            
            fprintf('.')
            m = MM(k);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%% Dimension reduction : the linear g with coordinate
            %%%%% aligned components
            % Construct LINEAR g and ACTIVE SUBSPACE f
            g_DGSM = SmoothFeatureMap.randn(RV,m);
            g_DGSM = Jg.DGSM_map(g_DGSM);
            Jf = CostFunction_Profile(g_DGSM,X{i,j},uX,guX);
            opts = CostFunction_Profile.OMP_default_opts();
            opts.maxit = 1000;
            opts.numberOfNewTermsPerIteration = NbOfNewTerms(j);
            [f_DGSM,Jf_iter_DGSM{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),opts,0);
            
            % Estimate the error on the training set
            error_H1_DGSM(i,j,k) = Jf.eval(f_DGSM);
            Jf.with_derivative = 0;
            error_L2_DGSM(i,j,k) = Jf.eval(f_DGSM);
            
            % Estimate the error on the validation set
            Jf_validation = CostFunction_Profile(g_DGSM,X_validation,uX_validation,guX_validation,1);
            error_H1_validation_DGSM(i,j,k) = Jf_validation.eval(f_DGSM);
            Jf_validation.with_derivative = 0;
            error_L2_validation_DGSM(i,j,k) = Jf_validation.eval(f_DGSM);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%% ACTIVE SUBSPACE : the linear g
            % Construct LINEAR g and ACTIVE SUBSPACE f
            g_AS = SmoothFeatureMap.randn(RV,m);
            g_AS = Jg.active_subspace_map(g_AS);
            Jf = CostFunction_Profile(g_AS,X{i,j},uX,guX);
            opts = CostFunction_Profile.OMP_default_opts();
            opts.maxit = 1000;
            opts.numberOfNewTermsPerIteration = NbOfNewTerms(j);
            [f_AS,Jf_iter_AS{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),opts,0);
            
            % Estimate the error on the training set
            error_H1_AS(i,j,k) = Jf.eval(f_AS);
            Jf.with_derivative = 0;
            error_L2_AS(i,j,k) = Jf.eval(f_AS);
            
            % Estimate the error on the validation set
            Jf_validation = CostFunction_Profile(g_AS,X_validation,uX_validation,guX_validation,1);
            error_H1_validation_AS(i,j,k) = Jf_validation.eval(f_AS);
            Jf_validation.with_derivative = 0;
            error_L2_validation_AS(i,j,k) = Jf_validation.eval(f_AS);
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%% The NONLINEAR g
            opts = CostFunction_FeatureMap.OMP_default_opts();
            opts.maxit = 1000;
%             opts.quasi_Newton_opts.compute_Sigma_every = 3;
            opts.numberOfNewTermsPerIteration = NbOfNewTerms(j);
            [g,Jg_iter{i,j,k},outputg{i,j,k}] = Jg.OMP_CV(g_AS,opts,0);
            outputg{i,j,k}.g = [];
            outputg{i,j,k}.QuasiNewton_J = [];
            outputg{i,j,k}.QuasiNewton_output = [];
            
            % Construct f
            Jf = CostFunction_Profile(g,X{i,j},uX,guX);
            opts = CostFunction_Profile.OMP_default_opts();
            opts.maxit = 1000;
            opts.numberOfNewTermsPerIteration = NbOfNewTerms(j);
            [f,Jf_iter{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),opts,0);
            
            % Estimate the error on the training set
            error_H1(i,j,k) = Jf.eval(f);
            Jf.with_derivative = 0;
            error_L2(i,j,k) = Jf.eval(f);
            
            % Estimate the error on the validation set
            Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,1);
            error_H1_validation(i,j,k) = Jf_validation.eval(f);
            Jf_validation.with_derivative = 0;
            error_L2_validation(i,j,k) = Jf_validation.eval(f);
            
        end
        fprintf('\n')
    end
    t = toc;
    disp(['------------------> ETA: ' datestr(datetime()+seconds(t*(K-i)),'DD:HH:MM:SS')])
end

t1=datetime();
disp(['Total time:' datestr(t1-t0,'DD:HH:MM:SS')])

%% SAVE

% save(['Runs/data/run_Borehole_' date '.mat'],'-v7.3')
% save(['Runs/data/run_Oscillator_' date '.mat'],'-v7.3')
% save(['Runs/data/run_Example4_' date '.mat'],'-v7.3')
save(['Runs/data/run_DeepComposed_' date '.mat'],'-v7.3')



