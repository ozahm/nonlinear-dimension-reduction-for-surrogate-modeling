%%

dim = 20;
u = cosOfNorm(dim);
RV = RandomVector(u.measure.measures);

%% Analyze the Quasi-Newton's method
K = 10;
MM = [1 5 10];

J_iter_linear = cell(K,length(MM));
J_iter_quadratic = cell(K,length(MM));


for j=1:length(MM)
    for i=1:K
        N = 100;
        X = RV.random(N);
        uX = u.eval(X);
        guX = u.evalGradient(X);
        Jg = CostFunction_FeatureMap(X,guX);
        
        opts = Jg.quasi_Newton_default_opts();
        opts.increment_tol = 1e-20;
        opts.dJ_tol = 1e-20;
        opts.maxit = 18;
        
        
        g_linear = SmoothFeatureMap.randn(RV,MM(j));
        [g_linear,J_iter_linear{i,j}] = Jg.quasi_Newton(g_linear,opts);
        
        opts.maxit = 40;
        
        g_quadratic = g_linear.get_marginExtended_FeatureMap(1:g_linear.reduced_margin.cardinal());
        [~,J_iter_quadratic{i,j}] = Jg.quasi_Newton(g_quadratic,opts);
    end
end

%% Plot

col = distinguishable_colors(length(MM));
for j=1:length(MM)
    semilogy([J_iter_linear{1,j} ; J_iter_quadratic{1,j}],'color',col(j,:))
    hold on
    for k=2:K
        semilogy([J_iter_linear{k,j} ; J_iter_quadratic{k,j}],'color',col(j,:),'HandleVisibility','off')
    end
end

semilogy([20 20],[0.0001 100],'k--')
hold off

legend(arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0), 'Location','SouthWest')
xlabel('Quasi-Newton iteration $k$')
ylabel('$\widehat{J}(g)$')
axis([1,40,0.001,1])


% my_matlab2tikz('Plot/CosOfNorm/','CosOfNorm_QuasiNewton.tex')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear all
% load('./Runs/data/run_CosOfNorm_05-Oct-2020.mat')



%% Plot J(g)

col = distinguishable_colors(3);
for k=1:5
    jj=1
    for i=1:length(MM)
        nia=20+[0:length(Jg_true{i,k})-1];
        if i==1
            nia = (nia-20)*20/19+20;
        end
        semilogy(nia,Jg_true{i,k},'color',col(jj,:),'LineStyle','-')
        hold on
        jj=jj+1;
    end
end
hold off
legend(arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0), 'Location','SouthWest')

xlabel('$\#\Lambda_{K}$')
ylabel('$J(g)$')
axis([20,45,0.01,1])

% my_matlab2tikz('Plot/CosOfNorm/','CosOfNorm_OMP_g.tex')



%% Plot J(f)
% per OMP iteration
col = distinguishable_colors(numel(MM));


for k=1:K
    kkkk=1;
    for i=1:numel(MM)
        semilogy(Jfg_true_L2{i,k},'color',col(kkkk,:),'LineStyle','-')
        kkkk=kkkk+1;
        hold on
    end
    semilogy(Jv_true_L2{k},'color',[0 0 0],'LineStyle','-','linewidth',1.2)
end

hold off
L = arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0);
legend([L,{'$m=d$'}], 'Location','SouthEast')
xlabel('OMP iteration')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

axis([1 50 0.004 0.4])


my_matlab2tikz('Plot/CosOfNorm/','CosOfNorm_OMP_f_Iter.tex')

%% per \#\Gamma_L

col = distinguishable_colors(numel(MM));


for k=1:K
    kkkk=1;
    for i=1:numel(MM)
        L = cellfun(@(x)size(x.coef,1),output_f{i,k}.f);
        semilogy(L,Jfg_true_L2{i,k},'color',col(kkkk,:),'LineStyle','-')
        kkkk=kkkk+1;
        hold on
    end
    L = cellfun(@(x)size(x.coef,1),output_v{k}.f);
    semilogy(L,Jv_true_L2{k},'color',[0 0 0],'LineStyle','-','linewidth',1.2)
end

hold off
L = arrayfun(@(x)['$m=' num2str(x) '$'],MM,'UniformOutput',0);
legend([L,{'$m=d$'}], 'Location','SouthEast')
xlabel('$\#\Gamma_L$')
ylabel('$\mathbb{E}[\|u(X)-f\circ g(X)\|^2]$')

axis([1 300 0.001 0.4])


my_matlab2tikz('Plot/CosOfNorm/','CosOfNorm_OMP_f.tex')



