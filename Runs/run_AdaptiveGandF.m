%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Initialize the model

% u = BoreholeFunction(); % m=d is the best...
% u = Example4(); % Pas convaincant...
u = DeepComposed(4); % level=3: un trou a m=2 (avec level=4 c'est vraiment convaincant!!)
% u = Oscillator();  % 


% %%%%% or the bridge:
% dim = 30; % 50 looks nice
% indEigs = 1;
% hmax = 3;
% u = MorandiBridge(dim,indEigs,hmax);
% RV = RandomVector(u.measure.measures);
% u.plotMesh()
% disp('done.')

%%
RV = RandomVector(u.measure.measures);
dim = u.dim;

%% Validation set...

N_validation = 2000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);
Jg_validation  = CostFunction_FeatureMap(X_validation,guX_validation);

%% Draw the sample {Xi,u(Xi),grad_u(Xi)}
t0 = datetime();

K = 20; % number of trials 
NN = [30 100 300]; % sample size 

% NN = 50;
% MM = [dim 10:-2:1 1];  % dimensions to try 
MM = [16 12 8 6 4 2 1];  % dimensions to try 
% MM = [dim:-1:1];  % dimensions to try 

X = cell(K,numel(NN));
X_bis = cell(K,numel(NN));

% g_store = cell(K,numel(NN),numel(MM));
output_g = cell(K,numel(NN),numel(MM));
output_f_NewSample_GradFree = cell(K,numel(NN),numel(MM));
output_f_NewSample = cell(K,numel(NN),numel(MM));
output_f_OldSample_GradFree = cell(K,numel(NN),numel(MM));
output_f_OldSample = cell(K,numel(NN),numel(MM));

Jg_adapt_validation = zeros(K,numel(NN),numel(MM));
% error_validation = zeros(K,numel(NN),numel(MM));
% error_GradFree_validation = zeros(K,numel(NN),numel(MM));
error_SameSample_validation = zeros(K,numel(NN),numel(MM));
error_SameSample_GradFree_validation = zeros(K,numel(NN),numel(MM));

for i=1:K
    tic
    disp(['Trial #' num2str(i) ' over ' num2str(K)])
    
    for j=1:length(NN)
        
        N = NN(j);
        fprintf(['    N=' num2str(N)])
        
        
        X{i,j} = RV.random(N);
        uX = u.eval(X{i,j});
        guX = u.evalGradient(X{i,j});
        
        Jg  = CostFunction_FeatureMap(X{i,j},guX);
        
        X_bis{i,j} = RV.random(N);
        uX_bis = u.eval(X_bis{i,j});
        guX_bis = u.evalGradient(X_bis{i,j});
        
        
        
        for k=1:length(MM)
            
            m = MM(k);
            fprintf('.')
            
            % Get the 3 feature maps (linear, quadratic, cubic)
            opts = Jg.OMP_default_opts();
            g = SmoothFeatureMap.randn(RV,m);
            [g,~,output_g{i,j,k}] = Jg.OMP_CV(g,opts,0);
            
            output_g{i,j,k}.g = {};
            output_g{i,j,k}.nbCoef = size(g.coef,1);
            output_g{i,j,k}.QuasiNewton_J = {};
            output_g{i,j,k}.QuasiNewton_output = {};
            
            Jg_adapt_validation(i,j,k) = Jg_validation.eval(g);
            
            
%             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ON THE NEW SAMPLE
%             Jf = CostFunction_Profile(g,X_bis{i,j},uX_bis,guX_bis);
%             Jf.with_derivative = 1;
%             [f,~,output_f_NewSample{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
%             output_f_NewSample{i,j,k}.f = {};
%             output_f_NewSample{i,j,k}.nbCoef = size(f.coef,1);
%             
%             Jf.with_derivative = 0;
%             [f_GradFree,~,output_f_NewSample_GradFree{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
%             output_f_NewSample_GradFree{i,j,k}.f = {};
%             output_f_NewSample_GradFree{i,j,k}.nbCoef = size(f_GradFree.coef,1);
%             
%             % Estimate the error on the validation set
%             Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
%             Jf_validation.with_derivative = 0;
%             error_validation(i,j,k) = Jf_validation.eval(f);
%             error_GradFree_validation(i,j,k) = Jf_validation.eval(f_GradFree);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SAME SAMPLE AS FOR G
            Jf = CostFunction_Profile(g,X{i,j},uX,guX);
            Jf.with_derivative = 1;
            [f,~,output_f_OldSample{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
            output_f_OldSample{i,j,k}.f = {};
            output_f_OldSample{i,j,k}.nbCoef = size(f.coef,1);
            
            Jf.with_derivative = 0;
            [f_GradFree,~,output_f_OldSample_GradFree{i,j,k}] = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
            output_f_OldSample_GradFree{i,j,k}.f = {};
            output_f_OldSample_GradFree{i,j,k}.nbCoef = size(f_GradFree.coef,1);
            
            % Estimate the error on the validation set
            Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
            Jf_validation.with_derivative = 0;
            error_SameSample_validation(i,j,k) = Jf_validation.eval(f);
            error_SameSample_GradFree_validation(i,j,k) = Jf_validation.eval(f_GradFree);
            
           
            
        end
        fprintf('\n')
    end
    t = toc;
    disp(['------------------> ETA: ' datestr(datetime()+seconds(t*(K-i)),'DD:HH:MM:SS')])
end

t1=datetime();
disp(['Total time:' datestr(t1-t0,'DD:HH:MM:SS')])


%% SAVE
% clear Jf  Jf_validation Jg Jg_validation f g f_GradFree

% save(['Runs/data/run_AdaptiveGandF_Borehole_' date '.mat'],'-v7.3')
save(['Runs/data/run_AdaptiveGandF_DeepComposed16_' date '.mat'],'-v7.3')

%% BS plots


% Jg_poly_validation 
% error_validation 
% error_GradFree_validation 
% error_SameSample_validation 
% error_SameSample_GradFree_validation 

indK = 1;

semilogy(MM,squeeze(Jg_adapt_validation(indK,:,:)))


subplot(2,2,1)
semilogy(MM,squeeze(error_validation(indK,:,:)))
subplot(2,2,2)
semilogy(MM,squeeze(error_GradFree_validation(indK,:,:)))
subplot(2,2,3)
semilogy(MM,squeeze(error_SameSample_validation(indK,:,:)))
subplot(2,2,4)
semilogy(MM,squeeze(error_SameSample_GradFree_validation(indK,:,:)))
%%
subplot(1,2,1)
semilogy(MM,squeeze(error_validation(indK,:,:)))
axis([1 8 1e-5 1e-3])
subplot(1,2,2)
semilogy(MM,squeeze(Jg_adapt_validation(indK,:,:)))
axis([1 8 1e-5 1e-3])



