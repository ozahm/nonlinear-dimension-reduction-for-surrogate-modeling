%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Initialize the model

u = BoreholeFunction();
% u = Example4();
% u = DeepComposed(3);
% u = Oscillator();

RV = RandomVector(u.measure.measures);
dim = u.dim;

% %% Initialize the model
% 
% dim = 20; % 50 looks nice
% indEigs = 1;
% hmax = 3;
% u = MorandiBridge(dim,indEigs,hmax);
% RV = RandomVector(u.measure.measures);
% u.plotMesh()
% disp('done.')
%% Validation set...

N_validation = 2000;
X_validation = RV.random(N_validation);
uX_validation = u.eval(X_validation);
guX_validation = u.evalGradient(X_validation);
Jg_validation  = CostFunction_FeatureMap(X_validation,guX_validation);

%% Draw the sample {Xi,u(Xi),grad_u(Xi)}
t0 = datetime();

K = 20; % number of trials
NN = [50 200 500]; % sample size (NN = [30 60 100 300])
MM = [dim:-1:1];  % dimensions to try
% MM = dim;  % dimensions to try

X = cell(K,numel(NN));
X_bis = cell(K,numel(NN));

Jg_poly_validation = zeros(K,numel(NN),numel(MM),3);
error_validation = zeros(K,numel(NN),numel(MM),3);
error_GradFree_validation = zeros(K,numel(NN),numel(MM),3);
error_SameSample_validation = zeros(K,numel(NN),numel(MM),3);
error_SameSample_GradFree_validation = zeros(K,numel(NN),numel(MM),3);
NumberOfCoefForF = cell(K,numel(NN),numel(MM),3);

for i=1:K
    tic
    disp(['Trial #' num2str(i) ' over ' num2str(K)])
    
    for j=1:length(NN)
        
        N = NN(j);
        fprintf(['    N=' num2str(N)])
        
        
        X{i,j} = RV.random(N);
        uX = u.eval(X{i,j});
        guX = u.evalGradient(X{i,j});
        
        Jg  = CostFunction_FeatureMap(X{i,j},guX);
        
        X_bis{i,j} = RV.random(N);
        uX_bis = u.eval(X_bis{i,j});
        guX_bis = u.evalGradient(X_bis{i,j});
        
        
        
        for k=1:length(MM)
            
            m = MM(k);
            fprintf('.')
            
            
%             if m < dim
                % Get the 3 feature maps (linear, quadratic, cubic)
                [g_cubic,~,~,g_quadratic,g_linear] = Jg.cubicMap(RV,m);
                Jg_linear_validation = Jg_validation.eval(g_linear);
                Jg_quadratic_validation = Jg_validation.eval(g_quadratic);
                Jg_cubic_validation = Jg_validation.eval(g_cubic);
                
                Jg_poly_validation(i,j,k,:) = [Jg_linear_validation,Jg_quadratic_validation,Jg_cubic_validation];
                GG = {g_linear,g_quadratic,g_cubic};
%             elseif m==dim
%                 g_AS = SmoothFeatureMap.randn(RV,m);
%                 g_AS = Jg.active_subspace_map(g_AS);
%                 tmp = Jg_validation.eval(g_AS);
%                 Jg_poly_validation(i,j,k,:) = [tmp,tmp,tmp];
%                 GG = {g_AS,g_AS,g_AS};
%                 
%             end
            
            
            
            for l=1:length(GG)
                % Construct f
                g = GG{l};
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ON THE NEW SAMPLE
                Jf = CostFunction_Profile(g,X_bis{i,j},uX_bis,guX_bis);
                Jf.with_derivative = 1;
                f = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
                NumberOfCoefForF{i,j,k,l}.NewSample_WithGradient = size(f.coef);
                
                Jf.with_derivative = 0;
                f_GradFree = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
                NumberOfCoefForF{i,j,k,l}.NewSample_WithoutGradient = size(f_GradFree.coef);
                
                % Estimate the error on the validation set
                Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
                Jf_validation.with_derivative = 0;
                error_validation(i,j,k,l) = Jf_validation.eval(f);
                error_GradFree_validation(i,j,k,l) = Jf_validation.eval(f_GradFree);
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SAME SAMPLE AS FOR G
                Jf = CostFunction_Profile(g,X{i,j},uX,guX);
                Jf.with_derivative = 1;
                f = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
                NumberOfCoefForF{i,j,k,l}.OldSample_WithGradient = size(f.coef);
                
                Jf.with_derivative = 0;
                f_GradFree = Jf.OMP_CV(SmoothProfile.randn(m),[],0);
                NumberOfCoefForF{i,j,k,l}.OldSample_WithoutGradient = size(f_GradFree.coef);
                
                % Estimate the error on the validation set
                Jf_validation = CostFunction_Profile(g,X_validation,uX_validation,guX_validation,0);
                Jf_validation.with_derivative = 0;
                error_SameSample_validation(i,j,k,l) = Jf_validation.eval(f);
                error_SameSample_GradFree_validation(i,j,k,l) = Jf_validation.eval(f_GradFree);
                
                
            end
            
            %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT
            %             subplot(1,2,1)
            %             semilogy(MM,squeeze(Jg_poly_validation))
            %             subplot(1,2,2)
            %             semilogy(MM,squeeze(error_validation))
            %             legend({'Linear','Quadratic','Cubic'})
            %             pause(0.1)
            
        end
        fprintf('\n')
    end
    t = toc;
    disp(['------------------> ETA: ' datestr(datetime()+seconds(t*(K-i)),'DD:HH:MM:SS')])
end

t1=datetime();
disp(['Total time:' datestr(t1-t0,'DD:HH:MM:SS')])


%% SAVE

% save(['Runs/data/run_FixedG_newAdaptiveF_Borehole_' date '.mat'],'-v7.3')
save(['Runs/data/run_FixedG_newAdaptiveF_Oscillator_' date '.mat'],'-v7.3')

%% BS plots


% Jg_poly_validation
% error_validation
% error_GradFree_validation
% error_SameSample_validation
% error_SameSample_GradFree_validation

semilogy(MM,squeeze(Jg_poly_validation))

subplot(2,2,1)
semilogy(MM,squeeze(error_validation))
subplot(2,2,2)
semilogy(MM,squeeze(error_GradFree_validation))
subplot(2,2,3)
semilogy(MM,squeeze(error_SameSample_validation))
subplot(2,2,4)
semilogy(MM,squeeze(error_SameSample_GradFree_validation))
%%

indK = 1;
indN = 1;

subplot(1,2,1)
semilogy(MM,squeeze(error_SameSample_validation(indK,indN,:,:)))
% axis([1 8 1e-5 1e-3])
subplot(1,2,2)
semilogy(MM,squeeze(Jg_poly_validation(indK,indN,:,:)))
% axis([1 8 1e-5 1e-3])


%%

squeeze( error_validation(1,:,1,1) )
