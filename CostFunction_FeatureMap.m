classdef CostFunction_FeatureMap
    
    % Cost function for the nonlinear dimension reduction method
    % 
    %   J(g) = E[|| (Id - Pi_{\nabla g(X)}) \nabla u(X) ||^2]
    % 
    
    
    properties
        
        dim % input space dimension
        N % Sample size
        X % N-by-d array of doubles
        grad_uX % N-by-d array of doubles
        
    end
    
    methods
        %------------------------------------------------------------------
        function self = CostFunction_FeatureMap(X,grad_uX)
            
            self.X = X;
            self.grad_uX = grad_uX;
            self.N = size(X,1);
            self.dim = size(X,2);
            
        end
        %------------------------------------------------------------------
        function [J,gradJ] = eval(self, g, grad_GX, ind_subsample)
            % [J,gradJ] = eval(self, g, ind_subsample)
            % 
            % Evaluate the function J and its gradient.
            % g: SmoothFeatureMap
            % ind_subsample: N'-by-1 array of integer to subsample {X,graduX}
            
            % Evaluate the gradients of G (if not provided)
            if nargin<3
                [~,~,grad_GX] = g.eval(self.X);
            end
            
            N_ = self.N;
            grad_uX_ = self.grad_uX;
            
            % Subsample the sample
            if nargin >= 4
                N_ = numel(ind_subsample);
                grad_uX_ = grad_uX_(ind_subsample,:);
                grad_GX = grad_GX{ind_subsample};
            end
            
            % Compute J
            Proj_grad_uX = grad_uX_;
            for i=1:N_
                [Q,~] = qr(grad_GX{i}'*g.coef,0);
                Proj_grad_uX(i,:) = Proj_grad_uX(i,:) - (Q*(Q'*(Proj_grad_uX(i,:)')))';
            end
            J = norm(Proj_grad_uX,'fro')^2/N_;
            
            % Compute grad_J (if required)
            if nargout>=2
                
                coef = g.coef;
                HG = zeros(size(coef));
                SG = zeros(size(coef));
                
                for i=1:N_
                    B12 = grad_GX{i}';
                    A12 = grad_uX_(i,:)*B12;
                    GBG = B12*coef;
                    GBG = GBG'*GBG;
                    A12G_GBGm1 = (A12*coef)/GBG;
                    
                    HG = HG + A12'*A12G_GBGm1;
                    SG = SG + (B12'*(B12*coef))*(A12G_GBGm1'*A12G_GBGm1);
                    
                end
                
                gradJ = -2*HG + 2*SG;
                gradJ = gradJ/N_;
                gradJ = gradJ(:);
                
            end
            
        end
        %------------------------------------------------------------------
        function H = H(self,g,grad_GX)
            % H = H(self,g)
            % Computes H(g) = E[ (BG(X)G)^{-1}  A(X) ]
            
            if nargin<3
                [~,~,grad_GX] = g.eval(self.X);
            end
            coef = g.coef;
            H = zeros(numel(g.coef));
            for i=1:self.N
                
                A = self.grad_uX(i,:)*grad_GX{i}';
                A = A'*A;
                GBG = grad_GX{i}' * coef;
                GBG = GBG'*GBG;
                invGBG = inv( GBG );
                H = H + kron( invGBG , A );
                
            end
            H = H/self.N;
        end
        %------------------------------------------------------------------
        function [A,B] = get_Rayleigh(self,g,grad_GX)
            % H = H(self,g)
            % Computes H(g) = E[ (BG(X)G)^{-1}  A(X) ]
            
            if nargin<3
                [~,~,grad_GX] = g.eval(self.X);
            end
            
            A = cell(self.N,1);
            B = cell(self.N,1);
            for i=1:self.N
                
                A{i} = self.grad_uX(i,:)*grad_GX{i}';
                A{i} = A{i}'*A{i};
                
                B{i} = grad_GX{i}';
                B{i} = B{i}'*B{i};
                
            end
            
        end
        %------------------------------------------------------------------
        function HG = H_times(self,g,coef0,grad_GX)
            % HG = HtimesG(self,g,coef0)
            % Computes H(g)G = E[ (BG(X)G)^{-1} coef0 A(X) ]
            
            if nargin<4
                [~,~,grad_GX] = g.eval(self.X);
            end
            coef = g.coef;
            
            sz = size(coef0);
            coef0 = reshape(coef0,size(g.coef));
            HG = zeros(size(coef0));
            for i=1:self.N
                
                B12 = grad_GX{i}';
                A12 = self.grad_uX(i,:)*B12;
                GBG = B12*coef;
                GBG = GBG'*GBG;
                
                HG = HG + A12'*((A12*coef0)/GBG) ;
            end
            HG = HG/self.N;
            HG = reshape(HG,sz);
        end
        %------------------------------------------------------------------
        function Sigma = Sigma(self,g,grad_GX)
            % Computes Sigma(g) = E[ (GB(X)B)^{-1} (GA(X)G) (GB(X)B)^{-1}  B(X) ]
            
            if nargin<3
                [~,~,grad_GX] = g.eval(self.X);
            end
            coef = g.coef;
            
            Sigma = zeros(numel(g.coef));
            for i=1:self.N
                B = grad_GX{i}';
                B = B'*B;
                A12 = self.grad_uX(i,:)*grad_GX{i}';
                GBG = coef'*B*coef;
                GBGm1A12G = GBG\((A12*coef)');
                Sigma = Sigma + kron( GBGm1A12G*GBGm1A12G' , B );
            end
            Sigma = Sigma/self.N;
            
        end
        %------------------------------------------------------------------
        function SG = Sigma_times(self,g,coef0,grad_GX)
            % Sgm = SigmatimesG(self,g,coef0,grad_GX)
            % Computes Sigma(g)coef0 = E[ (GB(X)B)^{-1} (GA(X)G) (GB(X)B)^{-1}  coef0 B(X) ]
            
            if nargin<4
                [~,~,grad_GX] = g.eval(self.X);
            end
            coef = g.coef;
            
            sz = size(coef0);
            coef0 = reshape(coef0,size(g.coef));
            SG = zeros(size(coef0));
            for i=1:self.N
                B12 = grad_GX{i}';
                A12 = self.grad_uX(i,:)*B12;
                GBG = B12*coef;
                GBG = GBG'*GBG;
                A12G_GBGm1 = (A12*coef)/GBG;
                
                SG = SG + (B12'*(B12*coef0))*(A12G_GBGm1'*A12G_GBGm1);
            end
            SG = SG/self.N;
            SG = reshape(SG,sz);
            
        end
        %------------------------------------------------------------------
        function Sigma_diag = diag_Sigma(self,g,grad_GX)
            % function Sigma_diag = diag_Sigma(self,g,grad_GX)
            % Computes the diagonal of Sigma(g)
            
            
            if nargin<3
                [~,~,grad_GX] = g.eval(self.X);
            end
            coef = g.coef;
            
            Sigma_diag = zeros(numel(g.coef),1);
            for i=1:self.N
                B12 = grad_GX{i}';
                diagB = sum(B12.^2,1);
                A12 = self.grad_uX(i,:)*B12;
                GBG = B12*coef;
                GBG = GBG'*GBG;
                A12G_GBGm1 = (A12*coef)/GBG;
                diagA = A12G_GBGm1.^2;
                
                Sigma_diag = Sigma_diag + kron( diagA(:), diagB(:) );
            end
            Sigma_diag = Sigma_diag/self.N;
            
        end
        %------------------------------------------------------------------
        %----------------- OPTIMIZATION PROCEDURES ------------------------
        %------------------------------------------------------------------
        function [g,J_iter,output] = quasi_Newton(self,g,opts,grad_GX)
            
            if nargin<3
                opts = CostFunction_FeatureMap.quasi_Newton_default_opts();
            end
            if nargin<4
                [~,~,grad_GX] = g.eval(self.X);
            end
            if g.m == self.dim
                output.exit_flag = 'Identity map (m=dim)';
                J_iter = 0;
                identity_map = SmoothFeatureMap.identity(g.RV);
                output.coef_iter = {g.coef ; identity_map.coef};
                output.g = {g ; identity_map};
                g = identity_map;
                g = g.standardize_pushforward();
                return
            end
            
            g = g.standardize_pushforward();
            
            J_iter = zeros(opts.maxit,1);
            coef_iter = cell(opts.maxit,1);
            pcg_resvec = cell(opts.maxit,1);
            for i=1:(opts.maxit+1)
                
                % Compute the gradient
                [J,gradJ] = self.eval(g,grad_GX);
                J_iter(i) = J;
                coef_iter{i} = g.coef;
                
                if i>2 && abs(J_iter(i)-J_iter(i-2))/J_iter(i-2) <= opts.dJ_tol
                    J_iter = J_iter(1:i);
                    coef_iter = coef_iter(1:i);
                    pcg_resvec = pcg_resvec(1:i);
                    break
                end
                
                % Compute the increment
                if opts.compute_Sigma_every == 1
                    % Recompute Sigma at each time. In this case, it is 
                    % more efficient not to assemble Sigma.
                    Sigma_fun = @(x) self.Sigma_times(g,x,grad_GX);
                    Sigma_diag = self.diag_Sigma(g,grad_GX);
                else
                    % Recompute Sigma every "opts.compute_Sigma_every"
                    % iterations.
                    if mod(i,opts.compute_Sigma_every)==1
                        Sigma = self.Sigma(g,grad_GX);
                    end
                    
                    Sigma_diag = diag(Sigma) ;
%                     Sigma_diag  = Sigma_diag  + 1e-5*mean(abs(Sigma_diag))
                    Sigma_fun = @(x) Sigma*x ;% + x*1e-5*mean(abs(Sigma_diag));
                    
                end
                % Preconditioner
                ind = find(abs(Sigma_diag)>10*eps*max(abs(Sigma_diag)));
                PrecondFun = zeros(size(Sigma_diag));
                PrecondFun(ind) = 1./Sigma_diag(ind);
                PrecondFun = @(x) x.*PrecondFun;
                
                % solve
                warning('off','all')
                [dcoef,~,~,~,pcg_resvec{i+1}] = pcg(Sigma_fun,-1/2*gradJ,...
                    opts.solver_tol,opts.solver_maxit,PrecondFun);
                warning('on','all')
                
                % Update the iterate
                coef = g.coef(:);
                coefNew = coef + dcoef;
                coefNew = opts.update(coefNew);
                g = g.set_coef(coefNew);
                g = g.standardize_pushforward();
                
                if norm(coefNew-coef)/norm(coef) <= opts.increment_tol
                    i = i+1;
                    J_iter(i) = self.eval(g,grad_GX);
                    coef_iter{i} = g.coef;
                    
                    J_iter = J_iter(1:i);
                    coef_iter = coef_iter(1:i);
                    pcg_resvec = pcg_resvec(1:i);
                    break
                end
            end
            output.coef_iter = coef_iter;
            output.pcg_iter = pcg_resvec;
        end
        %------------------------------------------------------------------
        function [g,J_iter,output] = OMP(self,g,opts,grad_GX)
            
            if nargin<3
                opts = CostFunction_FeatureMap.OMP_default_opts();
            end
            if nargin<4
                [~,~,grad_GX] = g.eval(self.X);
            end
            
            if g.m == self.dim
                output.exit_flag = 'Identity map (m=dim)';
                identity_map = SmoothFeatureMap.identity(g.RV);
                J_iter = 0;
                output.g = {g ; identity_map};
                output.QuasiNewton_J = {};
                output.QuasiNewton_output = {};
                g = identity_map;
                g = g.standardize_pushforward();
                return
            end
            opts_QN = opts.quasi_Newton_opts;
            
            % Compute grad_GX_margin
            [~,~,grad_GX_margin] = eval_on_reduced_margin(g,self.X);
            
            J_iter = zeros(opts.maxit+1,1);
            output.g =  cell(opts.maxit+1,1);
            output.QuasiNewton_J = cell(opts.maxit+1,1);
            output.QuasiNewton_output = cell(opts.maxit+1,1);
            
            for i=1:opts.maxit
                
                % Solve on the active-set (the lower-set)
                [g,J_iter_QN,output_QN] = quasi_Newton(self,g,opts_QN,grad_GX);
%                 g = g.standardize_pushforward();
                J_iter(i) = J_iter_QN(end);
                output.QuasiNewton_J{i} = J_iter_QN;
                output.QuasiNewton_output{i} = output_QN;
                output.g{i} = g;
                
                if i>1 && J_iter(i) > (1-opts.dJ_tol)*J_iter(i-1) 
                    output.exit_flag = 'dJ_tol';
                    break
                end
                
                
                % Compute grad_Jg_margin
                g_marginExtended = g.get_marginExtended_FeatureMap();
                grad_GX_marginExtended = cellfun(@(x,y)[x;y],grad_GX,grad_GX_margin,'UniformOutput',0);
                [~,grad_J_marginExtended] = self.eval(g_marginExtended,grad_GX_marginExtended);
                
                % Find the index of the highest gradient
                grad_J_marginExtended = reshape(grad_J_marginExtended,g_marginExtended.G.cardinal(),g.m);
                grad_J_marginExtended = sum(grad_J_marginExtended.^2,2);
                [grad_J_marginExtended,ind] = sort(abs(grad_J_marginExtended),'descend');
                
                nbOfNewParameter = 1+sum( cumsum(grad_J_marginExtended)/sum(grad_J_marginExtended) < opts.OMP_pctageOfgradRecovery );
                ind = ind(1:min(length(ind)-g.G.cardinal(),min(nbOfNewParameter,opts.numberOfNewTermsPerIteration)));
                
                if max(ind) <= g.G.cardinal()
                    output.exit_flag = 'Highest gradient is not in the reduced margin.';
                    break
                end
                ind = ind - g.G.cardinal();
                ind(ind<=0) = [];
                
                % Update g
                [g,new_in_margin] = g.get_marginExtended_FeatureMap(ind);
                if any(any( new_in_margin.array' > cellfun(@(H)max(H.indices),g.G.bases.bases) ))
                    output.exit_flag = 'Explosion of the polynomial order';
                    J_iter = J_iter(1:i);
                    output.g = output.g(1:i);
                    output.QuasiNewton_J = output.QuasiNewton_J(1:i);
                    output.QuasiNewton_output = output.QuasiNewton_output(1:i);
                    return
                end
                % Add ind in the new lower-set
                grad_GX = cellfun(@(x,y)[x;y(ind,:)],grad_GX,grad_GX_margin,'UniformOutput',0);
                
                % Remove ind from the old margin
                l = size(grad_GX_margin{1},1);
                grad_GX_margin = cellfun(@(x) x(setdiff(1:l,ind),:) ,grad_GX_margin,'UniformOutput',0);
                
                % Add the new margin terms in the current margin
                [~,~,grad_GX_newMargin] = eval_on_reduced_margin(g,self.X,new_in_margin);
                grad_GX_margin = cellfun(@(x,y)[x;y],grad_GX_margin,grad_GX_newMargin,'UniformOutput',0);
                
            end
            
            if i==opts.maxit
                output.exit_flag = 'Maximum number of iteration attained.';
            end
            if ~isfield(output,'exit_flag')
                output.exit_flag = 'Linear feature map';
            end
            if isempty(i)
                i = 0;
            end
            i = i+1;
            
            % Solve one last time on the lower-set
            [g,J_iter_QN,output_QN] = quasi_Newton(self,g,opts_QN,grad_GX);
%             g = g.standardize_pushforward();
            J_iter(i) = J_iter_QN(end);
            output.g{i} = g;
            output.QuasiNewton_J{i} = J_iter_QN;
            output.QuasiNewton_output{i} = output_QN;
            
            % Finalize the output
            J_iter = J_iter(1:i);
            output.g = output.g(1:i);
            output.QuasiNewton_J = output.QuasiNewton_J(1:i);
            output.QuasiNewton_output = output.QuasiNewton_output(1:i);
        end
        %------------------------------------------------------------------
        function [g,J_iter,output] = OMP_CV(self,g,opts,display)
            
            if nargin<3
                opts = CostFunction_FeatureMap.OMP_default_opts();
            end
            if nargin<4
                display = 1;
            end
            % Determine maxit by making sure we do not overfit
            N_train = floor(self.N*(opts.nb_CV_fold-1)/opts.nb_CV_fold);
            maxit_max = floor(0.8*(N_train/g.m-1)*self.dim / opts.numberOfNewTermsPerIteration);
            opts.maxit = min( maxit_max , opts.maxit);
            
            % Run the nu-fold method to determine "maxit"
            J_train = cell(opts.nb_CV_fold,1);
            J_test = cell(opts.nb_CV_fold,1);
            exit_flag_test = cell(opts.nb_CV_fold,1);
            perm = randperm(self.N);
            for i=1:opts.nb_CV_fold
                
                if display
                    disp(['Cross-validation for g: ' num2str(i) '/' num2str(opts.nb_CV_fold)])
                end
                
                % train/test splitting
                fold_size = floor(self.N/opts.nb_CV_fold);
                test = (i-1)*fold_size+1:i*fold_size;
                train = setdiff(1:self.N, test);
                test = perm(test);
                train = perm(train);
                
                % Training
                X_train = self.X(train,:);
                guX_train = self.grad_uX(train,:);
                J_train{i} = CostFunction_FeatureMap(X_train,guX_train);
                [~,J_train{i},output_train] = J_train{i}.OMP(g,opts);
                exit_flag_test{i} = output_train.exit_flag;
                
                % Test
                X_test = self.X(test,:);
                guX_test = self.grad_uX(test,:);
                J_test{i} = CostFunction_FeatureMap(X_test,guX_test);
                J_test{i} = cellfun(@(g) J_test{i}.eval(g), output_train.g );
                
            end
            
            % Determine the maxit which minimizes J_test
            maxit = zeros(max(cellfun(@(x)numel(x),J_test)),1);
            for i=1:length(maxit)
                tmp = cellfun(@(x)x(min(i,numel(x))),J_test,'UniformOutput',1);
                maxit(i) = mean(log(tmp));
            end
            [~,maxit] = min(maxit);
            
            
            % Run the complete algorithm with "maxit"
            opts.maxit = maxit;
            opts.dJ_tol = 1e-10;
            [g,J_iter,output] = self.OMP(g,opts);
            output.maxit_CV = maxit;
            output.J_test = J_test;
            output.exit_flag_test = exit_flag_test;
        end
        %------------------------------------------------------------------
        function g = active_subspace_map(self,g)
            
            % Extract the (only) relevant info from g
            m = g.m;
            RV = g.RV;
            
            % Active subspace (generalized eigenproblem)
            g = SmoothFeatureMap.randn(RV,1);
            Sigma = self.Sigma(g);
            Sigma = Sigma(2:end,2:end);
            H = self.H(g);
            H = H(2:end,2:end);
            Sigma12 = chol(Sigma);
            H = Sigma12\(H/(Sigma12'));
            [U,~] = svd(H);
            U = Sigma12'\U(:,1:m);
            
            % Finalize g
            g = SmoothFeatureMap.identity(RV);
            g.coef = [zeros(1,m) ; U];
            g.m = m;
            g = g.standardize_pushforward();
        end
        %------------------------------------------------------------------
        function [g_linear,J_iter,output] = linearMap(self,RV,m)
            
            g_linear = SmoothFeatureMap.randn(RV,m);
            [g_linear,J_iter,output] = self.quasi_Newton(g_linear);
            
        end
        %------------------------------------------------------------------
        function [g_quadratic,J_iter,output,g_linear] = quadraticMap(self,RV,m)
            
            [g_linear,J_iter_linear,output_linear] = linearMap(self,RV,m);
            g_quadratic = g_linear.get_marginExtended_FeatureMap(1:g_linear.reduced_margin.cardinal());
            [g_quadratic,J_iter_quadratic,output_quadratic] = self.quasi_Newton(g_quadratic);
            
            J_iter = {J_iter_linear;J_iter_quadratic};
            output = {output_linear;output_quadratic};
            
        end
        %------------------------------------------------------------------
        function [g_cubic,J_iter,output,g_quadratic,g_linear] = cubicMap(self,RV,m)
            
            [g_quadratic,J_iter_quadratic,output_quadratic,g_linear] = quadraticMap(self,RV,m);
            g_cubic = g_quadratic.get_marginExtended_FeatureMap(1:g_quadratic.reduced_margin.cardinal());
            [g_cubic,J_iter_cubic,output_cubic] = self.quasi_Newton(g_cubic);
            
            J_iter = {J_iter_quadratic;J_iter_cubic};
            output = {output_quadratic;output_cubic};
            
        end
        %------------------------------------------------------------------
        function g = DGSM_map(self,g)
            
            % Extract the (only) relevant info from g
            m = g.m;
            RV = g.RV;
            
            % Compute DGSM (multiplied by variance of RV)
            std_RV = g.RV.std();
            std_RV = [std_RV{:}];
            Hii = sum(self.grad_uX.^2)/self.N;
            Hii = Hii.*std_RV;
            
            % Create the extractor to apply to g.coef
            [~,perm] = sort(Hii,'descend');
            U = eye(g.dim);
            U = U(:,perm(1:m));
            
            % Finalize g
            g = SmoothFeatureMap.identity(RV);
            g.coef = g.coef*U;
            g.m = m;
            g = g.standardize_pushforward();
        end
        %------------------------------------------------------------------
    end
    methods (Static)
        %------------------------------------------------------------------
        function opts = quasi_Newton_default_opts()
            opts = struct( ...
                'maxit', 30, ...
                'increment_tol', 1e-10,...
                'dJ_tol', 1e-10, ...
                'update',@(x)x,...
                'compute_Sigma_every',2,...
                'solver_tol', 1e-4,...
                'solver_maxit', 50);
        end 
        %------------------------------------------------------------------
        function opts = OMP_default_opts()
            opts = struct( ...
                'maxit', 300, ...
                'dJ_tol', 1e-5, ...
                'nb_CV_fold',5, ...
                'numberOfNewTermsPerIteration',20,...
                'OMP_pctageOfgradRecovery',0.3,...
                'quasi_Newton_opts',CostFunction_FeatureMap.quasi_Newton_default_opts());
        end 
        %------------------------------------------------------------------
    end
    
end
            