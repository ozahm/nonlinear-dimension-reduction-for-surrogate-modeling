%% Initialize the problem
clc
clear all
addpath(genpath('.'));

%% Choose the function
% u = BoreholeFunction();
u = cosOfNorm(20);
% u = Oscillator();
% u = Example4();


% %% Or the bridge example
% 
% dim = 30;
% indEigs = 1;
% hmax = 3;
% u = MorandiBridge(dim,indEigs,hmax);
% RV = RandomVector(u.measure.measures);
% u.plotMesh()
% disp('done.')

RV = RandomVector(u.measure.measures);

%% Initialize J

N = 100;
X = RV.random(N);
guX = u.evalGradient(X);
J = CostFunction_FeatureMap(X,guX);

N_validation = 1000;
X_validation = RV.random(N_validation);
guX_validation = u.evalGradient(X_validation);
J_validation = CostFunction_FeatureMap(X_validation,guX_validation);

%% Compute g

m = 3;
g = SmoothFeatureMap.randn(RV,m);

opts = CostFunction_FeatureMap.OMP_default_opts();
opts.maxit = 30;
opts.quasi_Newton_opts.compute_Sigma_every = 10;
opts.numberOfNewTermsPerIteration = 1;

[g,J_iter,output] = J.OMP_CV(g,opts);

J_iter_validation = cellfun(@(g)J_validation.eval(g),output.g);
cardG_iter = cellfun(@(g)g.G.cardinal(),output.g);
semilogy(cardG_iter,[J_iter J_iter_validation])



%% Compute g for different m

M = [1 5 10];

opts = CostFunction_FeatureMap.OMP_default_opts();

J_iter = cell(length(M),1);
output = cell(length(M),1);
g = cell(length(M),1);
for i=1:length(M)
    m = M(i)
    opts.maxit = floor(J.N);
    g{i} = SmoothFeatureMap.randn(RV,m);
    [g{i},J_iter{i},output{i}] = J.OMP(g{i},opts);
end

% Evaluate the error on the validation set
Jg_validation = cell(length(M),1);
for i=1:length(M)
    Jg_validation{i} = zeros(length(output{i}.g),1);
    for j=1:length(output{i}.g)
        Jg_validation{i}(j) = J_validation.eval( output{i}.g{j} );
    end
end

%% Plot

colors = distinguishable_colors(length(M));
for i=1:length(M)
    semilogy(0:(length(J_iter{i})-1),J_iter{i},'color',colors(i,:))
    hold on
    semilogy(0:(length(Jg_validation{i})-1),Jg_validation{i},'color',colors(i,:),'LineStyle','--','HandleVisibility','off')
end
hold off

legend(arrayfun(@(x)['$m=' num2str(x) '$'],M,'UniformOutput',0))
xlabel('OMP iteration number')
ylabel('Cost function J')
















