function [g,Lambda] = Gradient_KDR(X,Y,g)

N = size(X,1);
%%% Choose the kernel for Y
kappaY = @(s,t) s*t';
%%% OR
% CovYinv = cov(Y);
% CovYinv = CovYinv + 0.05*trace(CovYinv)*eye(size(CovYinv));
% CovYinv = inv( CovYinv );
% kappaY = @(s,t) exp(-(s-t)*CovYinv*(s'-t'));
%%%

%%% Chosing the polynomial basis x->phi(x)
phiX = g.G.eval(X);


%%% Choose the kernel for phiX
% kappaPhiX = @(s,t) s*t';
% kappaPhiX_grad = @(s,t) t';
%%% OR
% deltaX = median(pdist(XY));
% kappaX = @(s,t) exp(-norm(s-t)^2/deltaX^2);
%%% OR
% CovXinv = cov(phiX);
% CovXinv = CovXinv + 0.005*trace(CovXinv)*eye(size(CovXinv));
% CovXinv = inv( CovXinv );
% kappaPhiX = @(s,t) exp(-(s-t)*CovXinv*(s'-t'));
% kappaPhiX_grad = @(s,t) -CovXinv*(s'-t')*exp(-(s-t)*CovXinv*(s'-t'));
%%% OR
kappaPhiX = @(s,t) exp(-(s-t)*(s'-t'));
kappaPhiX_grad = @(s,t) -2*(s'-t')*exp(-(s-t)*(s'-t'));
%%% OR
% kappaPhiX = @(s,t) exp(-s*t');
% kappaPhiX_grad = @(s,t) -(s')*exp(-s*t');

L = zeros(N);
K = zeros(N);
for i=1:N
    for j=1:N
        L(i,j) = kappaY(Y(i,:),Y(j,:));
        K(i,j) = kappaPhiX(phiX(i,:),phiX(j,:));
    end
end

%%% Remove the mean from L
% e = ones(N,1);
% L = L - 1/N*(L*e)*e';
% L = L - 1/N*e*(e'*L);
% L = (L+L')/2;

% Compute
[U,Lambda] = svd(L,'econ');
ind2keep = diag(Lambda)>1e-10*Lambda(1);
L12 = U(:,ind2keep)*diag(sqrt(diag(Lambda(ind2keep,ind2keep))));
eps = 0.001;
Km1L12 = (K+eps*trace(K)*eye(size(K))) \ L12;

M = zeros(size(phiX,2));
for i=1:N
    tmp = zeros(size(phiX,2),N);
    for j=1:N
        tmp(:,j) = kappaPhiX_grad(phiX(i,:),phiX(j,:));
    end
    tmp = tmp*Km1L12;
    M = M + tmp*tmp';
end

[U,Lambda] = svd(M);
Lambda = diag(Lambda);
% U=U';S
g.coef = U(:,1:g.m);
g = g.standardize_pushforward();

