clc
clear all
addpath(genpath('.'));

%% Choose the function

% u = BoreholeFunction(); type = 'Borehole';
u = cosOfNorm(20); type = 'cosOfNorm';

RV = RandomVector(u.measure.measures);

%% Compute the feature maps
% tic
total_degree = 2;
m = 1;
degreeF = 6;
% NN = [50 100 500 1000];
% NN = [30 60 150 300 500];
N = 500;
isKernelYGaussian = 1;

%%% Generate the data
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
Y = [uX,guX];


covY = cov(Y);
covYm12 = inv(chol(covY + trace(covY)*eye(size(covY))));
Y = (Y-mean(Y))*covYm12;

%%% Compute the feature maps
Jg  = CostFunction_FeatureMap(X,guX);
if total_degree==1
    g = Jg.linearMap(RV,m);
elseif total_degree==2
    g = Jg.quadraticMap(RV,m);
elseif total_degree==3
    g = Jg.cubicMap(RV,m);
end
Jf = CostFunction_Profile(g,X,uX,guX);
f = SmoothProfile(g.m,degreeF);
f = Jf.minimize(f);


gPCA = SupervisedPCA(X,Y,g,isKernelYGaussian);
Jf = CostFunction_Profile(gPCA,X,uX,guX);
fPCA = SmoothProfile(gPCA.m,degreeF);
fPCA = Jf.minimize(fPCA);

gKDR = KDR(Y,X,g,isKernelYGaussian);
Jf = CostFunction_Profile(gKDR,X,uX,guX);
fKDR = SmoothProfile(gKDR.m,degreeF);
fKDR = Jf.minimize(fKDR);


%%% Plot
%

Nplot = 1000;
Xplot = RV.random(Nplot);
uXplot = u.eval(Xplot);

gX = g.eval(Xplot);
gX_PCA = gPCA.eval(Xplot);
gX_KDR = gKDR.eval(Xplot);

%
lw = 2;
ms = 2;
subplot(1,3,1)
plot(gX_PCA,uXplot,'.b','MarkerSize',ms)
hold on
z = linspace(min(gX_PCA),max(gX_PCA),100)';
plot(z,fPCA.eval(z),'-r','linewidth',lw)
hold off
axis([min(gX_PCA),max(gX_PCA),min(uXplot),max(uXplot)])
title('KSPCA')
xlabel('$g(\mathbf{X})$')
legend('$u(\mathbf{X})$','$f\circ g(\mathbf{X})$')


subplot(1,3,2)
plot(gX_KDR,uXplot,'.b','MarkerSize',ms)
hold on
axis([min(gX_KDR),max(gX_KDR),min(uXplot),max(uXplot)])
z = linspace(min(gX_KDR),max(gX_KDR),100)';
plot(z,fKDR.eval(z),'-r','linewidth',lw)
hold off
title('KDR')
xlabel('$g(\mathbf{X})$')
% ylabel('$u(X)$')

subplot(1,3,3)
plot(gX,uXplot,'.b','MarkerSize',ms)
hold on
axis([min(gX),max(gX),min(uXplot),max(uXplot)])
z = linspace(min(gX),max(gX),100)';
plot(z,f.eval(z),'-r','linewidth',lw)
hold off
title('GNLDR')
xlabel('$g(\mathbf{X})$')
% ylabel('$u(X)$')







