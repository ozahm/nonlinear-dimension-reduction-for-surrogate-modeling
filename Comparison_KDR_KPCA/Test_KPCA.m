clc
clear all
addpath(genpath('.'));

%% Choose the function
u = BoreholeFunction();
% u = cosOfNorm(5);

%% Initialize the sample

N = 1000;

RV = RandomVector(u.measure.measures);
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);


%% Initialize the feature map

total_degree = 2;
m = 2;
g = SmoothFeatureMap(RandomVector(u.measure.measures),m,total_degree);

%% Supervised PCA
% Y = uX;
% Y = guX;
Y = [uX,guX];

gPCA = SupervisedPCA(X,Y,g);

%%% Plot
figure(1)
gX_PCA = gPCA.eval(X);
if m==1
    plot(gX_PCA,uX,'.b')
    [rho,pval] = corr(gX_PCA,uX,'Type','Spearman')
elseif m==2
%     plot3(gX_PCA(:,1),gX_PCA(:,2),uX,'.b')
    scatter(gX_PCA(:,1),gX_PCA(:,2),[],uX,'.')
    colorbar
end

title('Supervised PCA')

%% Kernel Supervised PCA
% Y = uX;
% Y = guX;
% Y = [uX,guX];

gX_KPCA = KernelSupervisedPCA(X,Y,m,0);

%%% Plot
figure(2)
if m==1
    plot(gX_KPCA,uX,'.b')
    [rho,pval] = corr(gX_KPCA',uX,'Type','Spearman')
elseif m==2
    plot3(gX_KPCA(1,:),gX_KPCA(2,:),uX,'.b')
%     scatter(gX_KPCA(1,:),gX_KPCA(2,:),[],uX,'.')
end
title('Kernel Supervised PCA')

%% Gradient KDR

if total_degree~=1
    warning('Works only for linear g...')
end
Y = uX;
gGKDR = Gradient_KDR(X,Y,g);

%%% Plot
gX_GKDR = gGKDR.eval(X);
figure(3)
if m==1
    plot(gX_GKDR,uX,'.b')
    [rho,pval] = corr(gX_GKDR,uX,'Type','Spearman')
elseif m==2
    plot3(gX_GKDR(:,1),gX_GKDR(:,2),uX,'.b')
end
title('Gradient KDR')
disp('Done.')

%% Our method

Jg  = CostFunction_FeatureMap(X,guX);
if total_degree==1
    g = Jg.linearMap(RV,m);
elseif total_degree==2
    g = Jg.quadraticMap(RV,m);
elseif total_degree==3
    g = Jg.cubicMap(RV,m);
end

%%% Plot
figure(10)
gX = g.eval(X);
if m==1
    plot(gX,uX,'.r')
    [rho,pval] = corr(gX,uX,'Type','Spearman')
elseif m==2
%     plot3(gX(:,1),gX(:,2),uX,'.r')
    scatter(gX(:,1),gX(:,2),[],uX,'.')
    colobar
end
title('Min J(g)')


%% GSIR

Y = uX;
% Y = guX;
% Y = [uX,guX];
epsK = 0.1;
epsL = epsK;
gGSIR = GSIR(X,Y,g,epsK,epsL);

%%% Plot
figure(4)
gX_GSIR = gGSIR.eval(X);
if m==1
    plot(gX_PCA,uX,'.b')
    [rho,pval] = corr(gX_GSIR,uX,'Type','Spearman')
elseif m==2
    plot3(gX_PCA(:,1),gX_PCA(:,2),uX,'.b')
%     scatter(gX_GSIR(:,1),gX_GSIR(:,2),[],uX,'.')
end

title('GSIR')

