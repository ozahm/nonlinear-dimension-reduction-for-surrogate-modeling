function U = HSIC_DR(X,Y,m,isKernelYGaussian)


if isa(m,'SmoothFeatureMap')
    g = m;
    m = g.m;
    X = g.G.eval(X);
    U = HSIC_DR(X,Y,m,isKernelYGaussian);
    g.coef = U;
    g = g.standardize_pushforward();
    U = g;
    return
end

N = size(X,1);
dim = size(X,2);
sigmaX = median(pdist(X));

%%% Gaussian radial basis function (RBF) for Y
if isKernelYGaussian
    sigmaY = median(pdist(Y));
    RBF_Y = @(d) exp(-d.^2./(sigmaY^2));
    GY = squareform(RBF_Y(pdist(Y))) + eye(N)*RBF_Y(0);
    GY = RemoveMeans(GY);
else
    GY = Y*Y';
    GY = RemoveMeans(GY);
end


U = randn(dim,m);
[U,~] = svd(U,'econ');

fun = @(U) J_HSIC(U,X,GY,sigmaX);

opts.gtol = 1e-5;
opts.xtol = 1e-20;
opts.ftol = 1e-20;
opts.mxitr = 3000;

U = SGPM(U,fun,opts);


end

function [F,G] = J_HSIC(U,X,GY,sigmaX)

N = size(X,1);
m = size(U,2);
G = zeros(size(U));

UX = U'*(X');
RBF_X = @(d) exp(-d.^2./(sigmaX^2));
GUX = squareform(RBF_X(pdist(UX'))) + eye(N)*RBF_X(0);

F = -trace(GY*GUX);

for i=1:N
    
    Xi = (X- X(i,:))';
    UXi = U'*Xi;
    D = (GY(i,:).*GUX(i,:))';
    UXi = UXi*spdiags(D,0,N,N);
    tmp = Xi*UXi';
    G = G + tmp;
    
end
G = (2/sigmaX^2)*G;

end


function L = RemoveMeans(L)
%%% Remove the mean from L
N = size(L,1);
e = ones(N,1);
L = L - 1/N*(L*e)*e';
L = L - 1/N*e*(e'*L);
L = (L+L')/2;

end