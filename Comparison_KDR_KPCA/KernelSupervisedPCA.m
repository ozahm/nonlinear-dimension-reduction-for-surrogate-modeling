function [gX,Lambda] = KernelSupervisedPCA(X,Y,m,typeOfKernel)

if nargin<3
    m = 1;
end
if nargin<4
    typeOfKernel = 1;
end

N = size(X,1);
dY = size(Y,2);

%%% Choose the kernel for Y
CovYinv = cov(Y);
CovYinv = CovYinv + 0.05*trace(CovYinv)*eye(dY);
CovYinv = inv( CovYinv );
kappaY = @(s,t) exp(-(s-t)*CovYinv*(s'-t'));
%%% OR
% kappaY = @(s,t) s*t';


if typeOfKernel==0
    kappaX = @(s,t) exp(-(s-t)*(s'-t'));
    
elseif typeOfKernel==1
    CovXinv = cov(X);
    CovXinv = CovXinv + 0.005*trace(CovXinv)*eye(size(CovXinv));
    CovXinv = inv( CovXinv );
    kappaX = @(s,t) exp(-(s-t)*CovXinv*(s'-t'));
    
elseif typeOfKernel==2
    kappaX = @(s,t) s*t';
end

%%% Assemble L
L = zeros(N);
K = zeros(N);
for i=1:N
    for j=1:N
        L(i,j) = kappaY(Y(i,:),Y(j,:));
        K(i,j) = kappaX(X(i,:),X(j,:));
    end
end

%%% Remove the mean from L
e = ones(N,1);
L = L - 1/N*(L*e)*e';
L = L - 1/N*e*(e'*L);
L = (L+L')/2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Assemble the matrix "Q ← K12HLHK21"
[UK,DK] = svd(K);
K12 = UK*diag(sqrt(diag(DK)))*UK';
Q = K12*L*K12;
[U,Lambda] = svd(Q);
Lambda = diag(Lambda);

% U = UK*diag(1./sqrt(diag(DK)))*U;
% U = U(:,1:m);

gX = U'*K12;
gX = gX(1:m,:);

end