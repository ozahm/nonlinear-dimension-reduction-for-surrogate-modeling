function [g,Lambda] = SupervisedPCA(X,Y,g,SupervisedPCA)


N = size(X,1);
dY = size(Y,2);

%%% Choose the kernel for Y
if SupervisedPCA
    epsL = 0.01;
    CovYinv12 = cov(Y);
    CovYinv12 = CovYinv12 + epsL*trace(CovYinv12)*eye(dY);
    [CovYinv12,D] = svd(CovYinv12);
    CovYinv12 = CovYinv12*diag(1./sqrt(diag(D)))*CovYinv12';
    Y = (Y-mean(Y))*CovYinv12;
    L = squareform(exp(-pdist(Y).^2/2)) + eye(N);
else
    L=Y*Y';
end

%%% Remove the mean from L
e = ones(N,1);
L = L - 1/N*(L*e)*e';
L = L - 1/N*e*(e'*L);
L = (L+L')/2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Assemble the matrix "Q = Φ(X)HLHΦ(X)"
phiX = g.G.eval(X);
Q = phiX'*L*phiX;

[U,Lambda] = svd(Q);
Lambda = diag(Lambda);
g.coef = U(:,1:g.m);
g = g.standardize_pushforward();

end