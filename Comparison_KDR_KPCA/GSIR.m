function [g,Lambda] = GSIR(X,Y,g,epsK,epsL)
% From LI AND CHIAROMONTE

N = size(X,1);
dY = size(Y,2);

%%% Choose the kernel for Y
CovYinv = cov(Y);
CovYinv = CovYinv + 0.05*trace(CovYinv)*eye(dY);
CovYinv = inv( CovYinv );
kappaY = @(s,t) exp(-(s-t)*CovYinv*(s'-t'));
%%% OR
% kappaY = @(s,t) s*t';



%%% Assemble L
L = zeros(N);
for i=1:N
    for j=1:N
        L(i,j) = kappaY(Y(i,:),Y(j,:));
    end
end

phiX = g.G.eval(X);
K = phiX*phiX';

%%% Remove the mean from L and K
e = ones(N,1);
L = L - 1/N*(L*e)*e';
L = L - 1/N*e*(e'*L);
L = (L+L')/2;
K = K - 1/N*(K*e)*e';
K = K - 1/N*e*(e'*K);
K = (K+K')/2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Assemble the matrix "Q = ..."

[UL,DL] = svd(L);
DL = diag(DL);
[UK,DK] = svd(K);
DK = diag(DK);
Q =   UK*diag( (DK./(epsK+DK)).^(3/2) )*UK';
Q = Q*UL*diag( (DL./(epsL+DL)) )*UL';

[U,Lambda] = svd(Q);

U = U * UK*diag( (1./(epsK+DK)) )*UK';

Lambda = diag(Lambda);
g.coef = U(1:size(g.coef,1),1:g.m);
g = g.standardize_pushforward();

end