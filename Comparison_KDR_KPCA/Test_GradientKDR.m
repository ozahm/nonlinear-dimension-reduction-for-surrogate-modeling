clc
clear all
addpath(genpath('.'));

%% Choose the function
u = BoreholeFunction();
% u = cosOfNorm(2);
% u = Oscillator();
% u = Example4();

RV = RandomVector(u.measure.measures);

%% Initialize the sample

N = 100;
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);

%%

% Y = uX;
Y = guX;
% Y = [uX guX];

%%% Choose the kernel for Y
% kappaY = @(s,t) s*t';
%%% OR
% deltaY = median(pdist(Y));
% kappaY = @(s,t) exp(-norm(s-t)^2/deltaY^2);
%%% OR
CovYinv = cov(Y);
CovYinv = CovYinv + 0.05*trace(CovYinv)*eye(size(CovYinv));
CovYinv = inv( CovYinv );
kappaY = @(s,t) exp(-(s-t)*CovYinv*(s'-t'));
%%%

%%% Chosing the polynomial basis x->phi(x)
total_degree = 3;
m = 4;
g = SmoothFeatureMap(RandomVector(u.measure.measures),m,total_degree);
phiX = g.G.eval(X);


%%% Choose the kernel for phiX
% kappaX = @(s,t) s*t';
%%% OR
% deltaX = median(pdist(XY));
% kappaX = @(s,t) exp(-norm(s-t)^2/deltaX^2);
%%% OR
kappaPhiX = @(s,t) exp(-(s-t)*(s'-t'));
kappaPhiX_grad = @(s,t) -(s'-t')*exp(-(s-t)*(s'-t'));
%%%


L = zeros(N);
K = zeros(N);
for i=1:N
    for j=1:N
        L(i,j) = kappaY(Y(i,:),Y(j,:));
        K(i,j) = kappaPhiX(phiX(i,:),phiX(j,:));
    end
end

[U,Lambda] = svd(L);
L12 = U*diag(sqrt(diag(Lambda)));
Km1L12 = K\L12;
%%

M = zeros(size(phiX,2));
for i=1:N
    tmp = zeros(size(phiX,2),N);
    for j=1:N
        tmp(:,j) = kappaPhiX_grad(phiX(i,:),phiX(j,:));
    end
    tmp = tmp*Km1L12;
    M = M + tmp*tmp';
end

[U,Lambda] = svd(M);
semilogy(diag(Lambda))

