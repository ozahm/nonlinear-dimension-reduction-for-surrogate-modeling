clc
clear all
addpath(genpath('.'));

%% Choose the function
% u = BoreholeFunction();
u = cosOfNorm(8);
RV = RandomVector(u.measure.measures);

%% Compute the feature maps

total_degree = 2;
m = 1;

N = 300;

X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);
Jg  = CostFunction_FeatureMap(X,guX);
if total_degree==1
    g = Jg.linearMap(RV,m);
elseif total_degree==2
    g = Jg.quadraticMap(RV,m);
elseif total_degree==3
    g = Jg.cubicMap(RV,m);
end


% N = 1000;
% X = RV.random(N);
% uX = u.eval(X);
% guX = u.evalGradient(X);


% Y = uX;
% Y = guX;
Y = [uX,guX];
covY = cov(Y);
covYm12 = inv(chol(covY + trace(covY)*eye(size(covY))));
Y = (Y-mean(Y))*covYm12;

% profile on
gPCA = SupervisedPCA(X,Y,g);
gKDR = KDR(Y,X,g);
% profile off
% profile viewer

%%% Plot
Nplot = 10000;
Xplot = RV.random(Nplot);
uXplot = u.eval(Xplot);

gX = g.eval(Xplot);
gX_PCA = gPCA.eval(Xplot);
gX_KDR = gKDR.eval(Xplot);

if m==1
    subplot(1,3,1)
    plot(gX_PCA,uXplot,'.b')
    title('Supervised PCA on polynomial basis')
    xlabel('$g_1(X)$')
    
    subplot(1,3,2)
    plot(gX_KDR,uXplot,'.b')
    title('KDR on polynomial basis')
    xlabel('$g_1(X)$')
    
    subplot(1,3,3)
    plot(gX,uXplot,'.b')
    title('min J(g)')
    xlabel('$g_1(X)$')
    
    
elseif m==2
    subplot(1,2,1)
    scatter3(gX_PCA(:,1),gX_PCA(:,2),uXplot,[],uXplot,'.')
%     scatter(gX_PCA(:,1),gX_PCA(:,2),[],uXplot,'.')
    title('Supervised PCA on polynomial basis')
    xlabel('$g_1(X)$')
    ylabel('$g_2(X)$')
    
    subplot(1,3,2)
    scatter3(gX_KDR(:,1),gX_KDR(:,2),uXplot,[],uXplot,'.')
%     scatter(gX(:,1),gX(:,2),[],uXplot,'.')
    title('KDR on polynomial basis')
    xlabel('$g_1(X)$')
    
    subplot(1,3,3)
    scatter3(gX(:,1),gX(:,2),uXplot,[],uXplot,'.')
%     scatter(gX(:,1),gX(:,2),[],uXplot,'.')
    title('min J(g)')
    xlabel('$g_1(X)$')
    ylabel('$g_2(X)$')
    
end







