clc
clear all
addpath(genpath('.'));

%% Choose the function
u = BoreholeFunction();
% u = cosOfNorm(5);

%% Initialize the sample

N = 100;

RV = RandomVector(u.measure.measures);
X = RV.random(N);
uX = u.eval(X);
guX = u.evalGradient(X);


Y = uX;
% Y = guX;
% Y = [uX,guX];

%%

m = 2
sigmaX = 1;
sigmaY = median(pdist(Y));
epsX = 0.01;

profile on
U = KDR(Y,X,m,sigmaX,sigmaY,epsX);
profile off
profile viewer










