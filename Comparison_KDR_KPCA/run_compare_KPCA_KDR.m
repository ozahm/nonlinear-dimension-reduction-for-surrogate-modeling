clc
clear all
addpath(genpath('.'));

%% Choose the function
% u = BoreholeFunction(); type = 'Borehole';
u = cosOfNorm(20); type = 'cosOfNorm';

disp(type)
RV = RandomVector(u.measure.measures);

%% Compute the feature maps

Kmax = 20; % Number of experiments
m = 1;
degreeF = 6;
isKernelYGaussian = 1;

if strcmp(type,'Borehole')
    NN = [30 60 150 300 500];
    DEGREE = [1 2];
elseif strcmp(type,'cosOfNorm')
    NN = [50 100 500 1000];
    DEGREE = 1;
end

%%% Validation set (to plot/compute errors)
N_val = 1000;
X_val = RV.random(N_val);
uX_val = u.eval(X_val);
guX_val = u.evalGradient(X_val);

%%% Initialize...
error = zeros(Kmax,length(DEGREE),length(NN));
errorPCA = zeros(Kmax,length(DEGREE),length(NN));
errorKDR = zeros(Kmax,length(DEGREE),length(NN));

%%% Run the loop
for k=1:Kmax
    disp(['Experiment n' num2str(k) ' out of ' num2str(Kmax)])
    
    for i=1:length(DEGREE)
        total_degree = DEGREE(i);
        
        for j=1:length(NN)
            N = NN(j);
            
            %%% Generate the data
            X = RV.random(N);
            uX = u.eval(X);
            guX = u.evalGradient(X);
            
            %%% Choose Y
            % Y = uX;
            % Y = guX;
            Y = [uX,guX];
            
            %%% Standardize Y
            covY = cov(Y);
            covYm12 = inv(chol(covY + trace(covY)*eye(size(covY))));
            Y = (Y-mean(Y))*covYm12;
            
            %%% Compute the feature maps
            Jg  = CostFunction_FeatureMap(X,guX);
            if total_degree==1
                g = Jg.linearMap(RV,m);
            elseif total_degree==2
                g = Jg.quadraticMap(RV,m);
            elseif total_degree==3
                g = Jg.cubicMap(RV,m);
            end
            Jf = CostFunction_Profile(g,X,uX,guX,1);
            f = Jf.minimize(SmoothProfile(m,degreeF));
            Jf_val = CostFunction_Profile(g,X_val,uX_val,guX_val,0);
            error(k,i,j) = Jf_val.eval(f);
            
            gPCA = SupervisedPCA(X,Y,g,isKernelYGaussian);
            Jf = CostFunction_Profile(gPCA,X,uX,guX);
            fPCA = Jf.minimize(SmoothProfile(m,degreeF));
            Jf_val = CostFunction_Profile(gPCA,X_val,uX_val,guX_val,0);
            errorPCA(k,i,j) = Jf_val.eval(fPCA);
            
            gKDR = KDR(Y,X,g,isKernelYGaussian);
            Jf = CostFunction_Profile(gKDR,X,uX,guX);
            fKDR = Jf.minimize(SmoothProfile(m,degreeF));
            Jf_val = CostFunction_Profile(gKDR,X_val,uX_val,guX_val,0);
            errorKDR(k,i,j) = Jf_val.eval(fKDR);
            
        end
    end
end

disp('Done.')
%%

% save(['Runs/data/run_compare_KPCA_KDR_' type '_m' num2str(m) '_deg0' date '.mat'],'-v7.3')

% load('Runs/data/run_compare_KPCA_KDR_Borehole_m1_27-Sep-2021.mat')
% load('Runs/data/run_compare_KPCA_KDR_cosOfNorm_m1_27-Sep-2021.mat')
load('Runs/data/run_compare_KPCA_KDR_cosOfNorm_m1_deg027-Sep-2021.mat')
% load('Runs/data/run_compare_KPCA_KDR_cosOfNorm_22-Sep-2021.mat')

%% Post-T


clc

i = 1;
total_degree = DEGREE(i);

sigmaU = 1;

disp([type ', degree ' num2str(total_degree)])
% N
tmp = arrayfun(@(x) ['& $N = ' num2str(x) '$ '],NN,'uniformoutput',0);
disp(['      ' tmp{:} '\\'])


% KSPCA
tmp = squeeze(errorPCA(:,i,:))/sigmaU;
tmp = arrayfun(@(x,y) ['& $' my_num2str(x,y) '$ '],mean(tmp),std(tmp),'uniformoutput',0);
disp(['KSPCA ' tmp{:} '\\ '])

% KDR
tmp = squeeze(errorKDR(:,i,:))/sigmaU;
tmp = arrayfun(@(x,y) ['& $' my_num2str(x,y) '$ '],mean(tmp),std(tmp),'uniformoutput',0);
disp(['KDR   ' tmp{:} '\\'])

% NLDR
tmp = squeeze(error(:,i,:))/sigmaU;
tmp = arrayfun(@(x,y) ['& $' my_num2str(x,y) '$ '],mean(tmp),std(tmp),'uniformoutput',0);
disp(['NLDR  ' tmp{:} '\\'])
